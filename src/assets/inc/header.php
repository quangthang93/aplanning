<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/variables.php'; ?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>株式会社エープランニング</title>
  <meta name="description" content="">
  <meta property="og:site_name" content="">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:type" content="website">
  <meta property="og:url" content="">
  <meta property="og:image" content="">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:image" content="">
  <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@100;200;300;400;700&display=swap" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;500;600;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $PATH;?>/assets/css/index.css">
</head>

<body>
  <div class="wrapper">
    <header class="header">
      <h1 class="header-logo link">
        <a href="/"><img src="<?php echo $PATH;?>/assets/images/common/header-logo.svg" alt=""></a>
      </h1>
      <div class="header-menu js-menu">
        <ul>
          <li><a class="link" href="/strength">当社の強み</a></li>
          <li><a class="link" href="/business">事業内容</a></li>
          <li><a class="link" href="/works">実績紹介</a></li>
          <li><a class="link" href="/philosophy">会社情報</a></li>
          <li><a class="link" href="/recruit">採用情報</a></li>
          <li class="sp-only"><a class="link" href="/news">ニュース</a></li>
          <li class="sp-only"><a class="link privacy" href="/privacy">プライバシーポリシー</a></li>
        </ul>
        <a href="/contact" class="header-menu--contact link">お問い合わせ</a>
        <p class="header-menu--copy sp-only">Copyright © 2021 A-Planning Inc. All rights reserved.</p>
      </div>
      <a class="header-ctrl js-menuTrigger" href="javascript:void(0);">
        <span></span>
        <span></span>
      </a>
    </header><!-- ./header -->

