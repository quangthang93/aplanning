		<footer class="footer">
      <div class="footer-inner">
        <div class="footer-top">
          <div class="footer-infor">
            <div class="footer-logo">
              <a class="link" href="/"><img src="<?php echo $PATH;?>/assets/images/common/footer-logo.svg" alt=""></a>
            </div>
            <p class="footer-address">〒 167-0043 東京都杉並区上荻一丁目15番1号</p>
            <p class="footer-tel">TEL: <a href="tel:03-5335-5828">03-5335-5828</a></p>
            <p class="footer-fax">FAX: 03-5347-0708</p>
          </div><!-- ./footer-infor -->
          <div class="footer-nav pc-only">
            <div class="footer-nav--item">
              <div class="footer-nav--direct"><a class="link" href="/business">事業内容</a></div>
              <ul class="footer-nav--links">
                <li><a class="link" href="">各種一般測量</a></li>
                <li><a class="link" href="">補償コンサルタント業務</a></li>
                <li><a class="link" href="">土地家屋調査測量業務</a></li>
                <li><a class="link" href="">建設コンサルタント</a></li>
                <li><a class="link" href="">環境調査</a></li>
                <li><a class="link" href="">一般労働者派遣事業</a></li>
                <li><a class="link" href="">有料職業紹介事業</a></li>
                <li><a class="link" href="/works">実績紹介</a></li>
              </ul>
            </div>
            <div class="footer-nav--item">
              <div class="footer-nav--direct"><a class="link" href="/philosophy">会社情報</a></div>
              <ul class="footer-nav--links">
                <li><a class="link" href="/philosophy">代表挨拶</a></li>
                <li><a class="link" href="/philosophy">企業理念・行動規範</a></li>
                <li><a class="link" href="/company">会社概要</a></li>
                <li><a class="link" href="/company">アクセス</a></li>
                <li><a class="link" href="/company">主な取引先</a></li>
                <li><a class="link" href="/company">資格保有者</a></li>
                <li><a class="link" href="/csr">社会貢献活動</a></li>
              </ul>
            </div>
            <div class="footer-nav--item">
              <div class="footer-nav--direct"><a class="link" href="/strength">当社の強み</a></div>
              <div class="footer-nav--direct"><a class="link" href="/news">ニュース</a></div>
              <div class="footer-nav--direct"><a class="link" href="/recruit">採用情報</a></div>
              <div class="footer-nav--direct"><a class="link" href="/contact">お問い合わせ</a></div>
              <div class="footer-nav--direct policy"><a class="link" href="/privacy">プライバシーポリシー</a></div>
            </div>
          </div><!-- ./footer-nav -->
        </div><!-- ./footer-top -->
        <div class="footer-copy">
          <p class="footer-copy--infor">
            Copyright © 2021 A-Planning Inc. All rights reserved.
          </p>
        </div><!-- ./footer-copy -->
    </div><!-- ./footer-inner -->
    </footer><!-- ./footer -->
  </div>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery.zmd.hierarchical-display.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/common.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/loading-top.js"></script>

  <!-- iFrame youtube -->
  <script>
    // 2. This code loads the IFrame Player API code asynchronously.
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/player_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    // after the API code downloads.
    var player;
    function onYouTubePlayerAPIReady() {
      player = new YT.Player('ytplayer', {
      width: '100%',
      height: '100%',
      videoId: 'wnqJNL2qwpk',
      playerVars: {
      'autoplay': 1,
      'showinfo': 0,
      'autohide': 1,
      'playsinline': 1,
      'loop': 1,
      'mute': 1,
      'controls': 0,
      'modestbranding': 1,
      'vq': 'hd1080'
      },
      events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
      }
      });
    }

    // 4. The API will call this function when the video player is ready.
    function onPlayerReady(event) {
      event.target.playVideo();
      player.mute(); // comment out if you don't want the auto played video muted
    }

    // 5. The API calls this function when the player's state changes.
    // The function indicates that when playing a video (state=1),
    // the player should play for six seconds and then stop.
    function onPlayerStateChange(event) {
      if (event.data == YT.PlayerState.ENDED) {
      player.seekTo(0);
      player.playVideo();
      }
      }
      function stopVideo() {
      player.stopVideo();
    }

  </script>
</body>

</html>