$(function() {
  scrollLoad()
  $(window).scroll(function() {
    scrollLoad()
  });
  /* --------------------
  GLOBAL VARIABLE
  --------------------- */
  // Selector
  // var $pageTop = $('.js-pageTop');

  // Init Value
  var breakpointSP = 767,
      breakpointTB = 1050,
      wWindow = $(window).outerWidth();


  /* --------------------
  FUNCTION COMMON
  --------------------- */
  // Menu trigger
  var triggerMenu = function() {
    $('.js-menuTrigger').click(function() {
      $('body').toggleClass('fixed');
      $(this).toggleClass('open');
      $('.js-menu').toggleClass('open');
    });
  }

  // Setting anchor link
  var anchorLink = function() {
    // Scroll to section
    $('a[href^="#"]').not("a[class*='carousel-control-']").click(function() {
      var href= $(this).attr("href");
      var hash = href == "#" || href == "" ? 'html' : href;
      var position = $(hash).offset().top - 100;
      $('body,html').stop().animate({scrollTop:position}, 1000);
      return false;
    });
  }

  // Setting anchor link
  var triggerPeoples = function() {
    $('.js-btnPeopleMore').click(function () {
      $('.js-peopleMore').show();
      $('.js-btnPeopleClose').show();
      $(this).hide().parent().css('margin-bottom', '150px');
    });

    $('.js-btnPeopleClose').click(function () {
      $('.js-btnPeopleMore').show();
      $('.js-peopleMore').hide();
      $(this).hide().parent().css('margin-bottom', '0');

      var position = $('.js-btnPeopleMore').offset().top - 100;
      $('body,html').stop().animate({scrollTop:position}, 500);
    });
  }


  /* --------------------
  INIT (WINDOW ON LOAD)
  --------------------- */
  // Run all script when DOM has loaded
  var init = function() {
    triggerMenu();
    anchorLink();
    triggerPeoples();
  }

  init();


  /* --------------------
  WINDOW ON RESIZE
  --------------------- */
  $(window).resize(function() {
    wWindow = $(window).outerWidth();
  });


  /* --------------------
  WINDOW ON SCROLL
  --------------------- */
  

});

function scrollLoad(){
 var scroll = $(this).scrollTop();
  $('.fadeup, .fadein').each(function() {
    var elemPos = $(this).offset().top;
    var windowHeight = $(window).height();
    if (scroll > elemPos - windowHeight + 100) {
      $(this).addClass('in');
    }
  });
}

/*AnchoLink to other Page*/
$(document).ready(function(linkOtherPage) {
var hash= window.location.hash
if ( hash == '' || hash == '#' || hash == undefined ) return false;
      var target = linkOtherPage(hash);
      headerHeight = 120;
      target = target.length ? target : $('[name=' + hash.slice(1) +']');
      if (target.length) {
        $('html,body').stop().animate({
          scrollTop: target.offset().top - 250 //offsets for fixed header
        }, 'linear');
      }
} );