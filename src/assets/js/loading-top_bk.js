$(function() {
  // global distance, unit as 'vw'

  var wWindow = $(window).outerWidth(),
      hWindow = $(window).outerHeight();

  // Selector
  var $matrixBox = $('.js-mvMatrix'),
      $overlayBox = $('.js-mvOverlay'),
      $header = $('.js-header.top'),
      $scroll = $('.js-scroll'),
      $aboutIntro = $('.js-aboutIntro');

  var distance,
      numOfPlusRow; // 4vw
  if (wWindow >= 1050) {
    distance = 5;
    numOfPlusRow = 10;
  } 
  else if (wWindow > 450  && wWindow < 1050) {
    distance = 5;
    numOfPlusRow = 15;
  } 
  else {
    distance = 12;
    numOfPlusRow = 12;
  }    

  var drawMatrix = function(distance, wWindow, hWindow) {

    // variable
    var wSquare = wWindow/100*distance;

    var numLineX = Math.round(wWindow / wSquare);
    var numLineY = Math.round(hWindow / wSquare);

    // append multi line
    for (var i = 1; i <= numLineX; i++) {
      $matrixBox.append('<span class="lineX lineHeight lineX' + i + '" style="left:'+ distance * i  + 'vw; transition-delay: '+ i*0.02 +'s;" ></span>');
    }
    for (var j = 1; j <= numLineY; j++) {
      $matrixBox.append('<span class="lineY lineWidth lineY' + j + '" style="top:'+ distance * j  + 'vw; transition-delay: '+ j*0.02 +'s;" ></span>');
    }

    // run animation for multi line
    setTimeout(function(){
      $('.lineWidth, .lineHeight').each(function () {
        $(this).addClass('in');
      });
    }, 10);

  };


  var drawOverlay = function(distance, wWindow, hWindow){
    var wSquare = wWindow/100*distance;

    // variable
    var numColumn = Math.round(100 / distance);
    var numRow = Math.round(hWindow / wSquare) + numOfPlusRow;
    for (var i = 1; i < numRow; i++) {
      for (var j = 1; j < numColumn; j++) {
        $overlayBox.append('<div style="width:'+ distance  + 'vw;height: '+ distance  + 'vw"></div>');
      }
    }
  }


  var removeOverlay = function () {
    var el = $overlayBox;
    var resetAndStart = function (el) {
      el.children().last().off('webkitAnimationEnd animationend');
      el.data('zmd.hierarchicalDisplay', false).hierarchicalDisplay();
    }
    el.data('action', 'hide');
    el.data('speed', 3); 
    el.data('animation-in', 'bounceIn');
    el.data('animation-out', 'zoomOut');

    el.children().each(function () {
        $(this).attr('class', '');
    });
    el.addClass('in');
    resetAndStart(el);
  }


  /* =======================
    Start animation loading
  ==========================*/

  // draw overlay
  drawOverlay(distance, wWindow, hWindow);

  // show logo
  $('.js-logo').fadeIn(1000);

  // hide logo
  setTimeout(function(){
    $('.js-logo').fadeOut(1000);
  }, 1500);

  // draw matrix line
  setTimeout(function(){
    drawMatrix(distance, wWindow, hWindow);
  }, 1800);

  // remove overlay
  setTimeout(function(){
    removeOverlay();
  }, 2500);

  // remove matrix line
  setTimeout(function(){
    $matrixBox.parent().fadeOut(500);
  }, 4000);

  // remove overlayBox
  setTimeout(function(){
    $overlayBox.parent().hide();
    $header.addClass('open');
  }, 5500);

  // show aboutIntro
  setTimeout(function(){
    $aboutIntro.addClass('open');
    $scroll.addClass('open');
  }, 6000);

  // remove overlayBox
  // setTimeout(function(){
  //   $('.js-mvImg').fadeOut(500);
  // }, 6000);

 

  /* --------------------
  WINDOW ON RESIZE
  --------------------- */
  // $(window).resize(function() {
  //   wWindow = $(window).outerWidth();
  //   drawMatrix();
  // });
});