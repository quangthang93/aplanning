<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="p-end p-company">
    <section class="p-end--banner">
      <div class="container">
        <h1 class="main-ttl">COMPANY<span>会社情報</span></h1>
      </div>
    </section><!-- ./p-recruit--banner -->
    <section class="p-company--cnt">
      <div class="container">
        <div class="p-company--ttlWrap type2">
          <div class="container">
            <h2 class="section-ttl2">ごあいさつ</h2>
          </div>
        </div>
        <div class="tabs-navWrapper">
          <ul class="tabs-nav js-tabsNav">
            <li class="tabs-item js-tabsItem active"><a href="/philosophy">ごあいさつ</a></li>
            <li class="tabs-item js-tabsItem"><a href="/company">会社概要</a></li>
            <li class="tabs-item js-tabsItem"><a href="/csr">CSR方針</a></li>
          </ul>
        </div><!-- ./tabs-navWrapper -->
        <div class="p-philosophy">
          <div class="p-philosophy--intro">
            <div class="container">
              <div class="p-philosophy--intro-cnt">
                <h3 class="section-ttl3">代表あいさつ</h3>
                <h4 class="section-ttl4">暮らしやすい社会を求めて</h4>
                <p class="desc3">株式会社エープランニングは、設立以来、常に社会のニーズを的確に捉え、時代の最先端を行く企画・提案により、クライアントへ安心と満足感を提供し、一歩ずつ実績を重ねてまいりました。「社会資本の円滑なる整備への寄与」を社業として、数多くの道路、鉄道、河川、ダム等に関する測量、補償、建設、環境、地質などのコンサルティング業務を通して、社会インフラ整備を技術的に支えております。 <br>「人・技術」を合言葉に社員の一人一人が技術の研鑽に励み、人が夢を持って暮らせるグローバルな真の社会サービスを提供する企業へと躍進してまいりたいと考えております。</p>
              </div>
              <div class="p-philosophy--intro-nameBox">
                <p class="p-philosophy--intro-pos">代表取締役社長</p>
                <p class="p-philosophy--intro-name">原口 浩如</p>
              </div>
              <div class="p-philosophy--intro-bg"></div>
            </div>
          </div><!-- ./p-philosophy--intro -->
          <div class="p-philosophy--msg">
            <div class="container">
              <ul class="p-philosophy--msg-list">
                <li class="p-philosophy--msg-item">
                  <h4 class="section-ttl4">流行に流されない都市計画</h4>
                  <p class="desc4">これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。</p>
                </li>
                <li class="p-philosophy--msg-item">
                  <h4 class="section-ttl4">見出しテキストが入ります</h4>
                  <p class="desc4">これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。</p>
                </li>
                <li class="p-philosophy--msg-item">
                  <h4 class="section-ttl4">見出しテキストが入ります</h4>
                  <p class="desc4">これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。</p>
                </li>
              </ul>
              <div class="p-philosophy--msg-sign">
                <p class="p-philosophy--msg-sign-label">代表取締役</p>
                <p class="p-philosophy--msg-sign-img">
                  <img src="<?php echo $PATH;?>/assets/images/company/sign.png" alt="">
                </p>
              </div>
            </div>
          </div><!-- ./p-philosophy--msg -->
          <div class="p-philosophy--management">
            <h3 class="section-ttl3">経営理念</h3>
            <div class="p-philosophy--management-cnt">
              <div class="p-philosophy--management-cnt-inner">
                <div class="p-philosophy--management-cnt-ttl">
                  人との絆を大切に、<br>
                  世に残る仕事を追求し、<br>
                  社会の懸け橋となり、<br>
                  不朽の功績をつなぐ
                </div>
                <div class="p-philosophy--management-cnt-des">
                  この文章はダミーテキストです。200文字程度入ります。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーテキストです。200文字程度入ります。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーテキストです。200文字程度入ります。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーテキストです。200文字程度入れています。
                </div>
              </div>
            </div>
            <!-- <ul class="p-philosophy--management-list">
              <li>わたしたちは出会うすべての人との絆を大切に、常にお客様のお役立ちを考え、真のパートナーシップカンパニーとなることを目指します。</li>
              <li>社員に対する万全の安全教育を徹底し、事故の発生を未然に防ぐべく最善を尽くします。</li>
              <li>コンプライアンスを旨とし、常に真摯で公平な経営を貫き、良き企業市民として雇用を創造し社会に貢献します。</li>
            </ul> -->
          </div><!-- ./p-philosophy--management -->
          <div class="p-philosophy--conduct">
            <h3 class="section-ttl3">行動規範</h3>
            <ol class="p-philosophy--conduct-list">
              <li>
                <h4 class="title-lv4"><span>1.</span>企業倫理と法令の遵守</h4>
                <p class="desc">社会からの信頼を獲得すべく、すべての役員と社員が常に公正さと高い倫理観、責任感を持ち、法令、その他の社会的規範等のルールについて十分な理解を図り、これを誠実に実行します。</p>
              </li>
              <li>
                <h4 class="title-lv4"><span>2.</span>人権の尊重</h4>
                <p class="desc">人権の尊重が企業にとって重要な社会的責任であるとの認識に立ち、その責任を果たすことにより、安心・安全で豊かな社会の実現をめざします。</p>
              </li>
              <li>
                <h4 class="title-lv4"><span>3.</span>職場環境の構築</h4>
                <p class="desc">社員が心身ともに健康で、個性や能力を最大限に発揮できることが、重要であると考えます。社員一人ひとりが、心身ともに健康で、常に働く喜びに満ち溢れ、社員の意思を尊重し、自らの強みを存分に発揮し、その能力を最大限に活かすことができる職場環境の構築に努めてまいります。</p>
              </li>
              <li>
                <h4 class="title-lv4"><span>4.</span>個人情報の取扱い</h4>
                <p class="desc">知的財産権の重要性を認識し、自らの権利を適切に保護、活用するとともに、他者の有効な権利を尊重し、これを不正に取得、使用しません。</p>
              </li>
              <li>
                <h4 class="title-lv4"><span>5.</span>個人情報の取扱い</h4>
                <p class="desc">すべての個人情報を適正に取扱うことが責務であると認識しており、個人情報の保護に関する法律を遵守し、すべての事業において取り扱う個人情報及び役員・従業員等の個人情報の保護に万全を尽くします。</p>
              </li>
              <li>
                <h4 class="title-lv4"><span>6.</span>公正で健全な企業活動</h4>
                <p class="desc">法令・社会規範・社内規定を遵守し、正しい倫理観に即した企業活動を行い、社会の厚い信頼を得られる存在であり続けます。</p>
              </li>
              <li>
                <h4 class="title-lv4"><span>7.</span>環境への配慮</h4>
                <p class="desc">生物多様性、環境の維持・保全の重要性を理解し、地球環境を守る日常活動を自主的かつ積極的に推進し、持続可能な社会の実現に貢献します。</p>
              </li>
              <li>
                <h4 class="title-lv4"><span>8.</span>地域社会との共生</h4>
                <p class="desc">地域の文化、習慣を尊重し地域の発展に資するとともに、地域の社会活動、災害救援活動、ボランティア活動の参加等広く社会貢献に努めます。</p>
              </li>
              <li>
                <h4 class="title-lv4"><span>9.</span>反社会的勢力及び団体への対処</h4>
                <p class="desc">反社会的な勢力や団体とは、一切の関係をもたず、不当な要求に対しては毅然とした態度で臨みます。</p>
              </li>
            </ol>
          </div><!-- ./p-philosophy--conduct -->
        </div><!-- ./p-philosophy -->
      </div>      
    </section>
  </div>
</main><!-- ./main -->

<div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>会社情報</li>
      <li>ごあいさつ</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>