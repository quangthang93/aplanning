<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="p-end">
    <div class="p-end--banner">
      <div class="container">
        <h1 class="main-ttl">PRIVACY POLICY<span>プライバシーポリシー</span></h1>
      </div>
    </div><!-- ./p-recruit--banner -->
    <div class="p-end--content">
      <div class="container">
        <div class="c-page">
          <div class="p-csr">
            <div class="title-lv2">個人情報保護方針</div>
            <p class="desc4">当社は、個人情報を適切に管理することを社会的な責務であると認識し、個人情報の取扱いに関する法令、国が定める指針その他の規範を遵守すると共に、個人情報保護に関する方針を下記のとおり定め、個人情報の適切な保護に努めます。</p>
            <ul class="p-csr--list">
              <li class="p-csr--item">
                <h3 class="p-csr--item-ttl">1.個人情報の取得および利用</h3>
                <p class="p-csr--item-des">当社は、利用目的を明確にした上で適法かつ公正な手段で個人情報を取得し、利用目的の範囲内で個人情報を利用すること及びそのための措置を講じます。</p>
              </li>
              <li class="p-csr--item">
                <h3 class="p-csr--item-ttl">2.個人情報の提供</h3>
                <p class="p-csr--item-des">当社は、本人の同意・承諾を得た場合、法令等により開示・提供が必要な場合、および利用目的の達成のために必要最小限の範囲内で発注者、委託先等の第三者に提供する場合を除き、第三者に個人情報を提供いたしません。</p>
              </li>
              <li class="p-csr--item">
                <h3 class="p-csr--item-ttl">3.個人情報の安全管理措置</h3>
                <p class="p-csr--item-des">当社は、取扱う個人情報の正確性・安全性の確保、個人情報への不正アクセス・漏えい・改ざん・滅失または毀損の防止、その他個人情報の安全を確保するために管理体制を整備し、適切な安全対策を実施致します。</p>
              </li>
              <li class="p-csr--item">
                <h3 class="p-csr--item-ttl">4.個人情報に関連する法令等の遵守</h3>
                <p class="p-csr--item-des">当社は、個人情報の取扱いにおいて、「個人情報の保護に関する法律」およびその他の関連法令・国が定める指針・その他の規範を遵守してまいります。</p>
              </li>
              <li class="p-csr--item">
                <h3 class="p-csr--item-ttl">5.???</h3>
                <p class="p-csr--item-des">当社の業務において個人情報を取り扱うすべての者は、個人情報保護の重要性を認識し、法規範および個人情報について適切な取得、利用、提供、廃棄等の方法を定めた社内規程に従い適切に取り扱います。</p>
              </li>
              <li class="p-csr--item">
                <h3 class="p-csr--item-ttl">6.個人情報保護管理体制の継続的改善の実施</h3>
                <p class="p-csr--item-des">当社は、個人情報保護方針を確実に実行するため、個人情報保護規定を策定し、従業員への周知・教育、管理の体制と仕組みについての継続的な改善を行ってまいります。</p>
              </li>
              <li class="p-csr--item">
                <h3 class="p-csr--item-ttl">7.個人情報に関する問い合わせについて</h3>
                <p class="p-csr--item-des">当社は、個人情報にかかわる苦情や相談があった場合、合理的かつ適正な判断により誠実に対応します。</p>
              </li>
            </ul>
            <div class="p-csr--item-des p-privacy__timestamp">
              <time>制定日：2020年（令和2年）4月1日<br />株式会社エープランニング<br />代表取締役　原口　浩如</time>
            </div>
            <div class="title-lv2">個人情報の取扱いについて</div>
            <p class="desc4">当社は、個人情報の利用目的を明確にした上で、その利用目的の範囲内で、個人情報の取得をいたします。また、これらの個人情報を、その利用目的の範囲内でのみ利用させていただきます。</p>
            <ul class="p-csr--list">
              <li class="p-csr--item">
                <h3 class="p-csr--item-ttl">1.個人情報の利用と提供について</h3>
                <p class="p-csr--item-des">(1)業務の実施に関する連絡、交渉、契約履行、履行請求、営業上の挨拶等のため<br />
                  (2)官公庁、公共サービス機関等に対し、各種申請、報告その他当社が事業活動を行うのに必要な業務の遂行のため<br />
                  (3)事業等の説明、同意取得など、近隣住民、地権者等に対する当社の事業活動のため必要な業務の遂行のため<br />
                  (4)社外および社内に対する広報活動およびこれに関連・付随する業務の遂行のため<br />
                  (5)当社の事業所に立ち入られる方および電話、Fax、メール等により連絡される方に対する身元確認および連絡等のため<br />
                  (6)当社の採用活動およびこれに付随・関連する手続のため<br />
                  (7)当社の雇用、採用等の人事労務管理および経理、総務等の業務のため<br />
                  なお、法令等に定める場合を除き、あらかじめご本人の同意を得ることなく、ご本人様の個人情報を第三者に開示、提供することはありません。
                </p>
              </li>
              <li class="p-csr--item">
                <h3 class="p-csr--item-ttl">2.第三者への提供について</h3>
                <p class="p-csr--item-des">当社は、本人の同意・承諾を得た場合、法令等により開示・提供が必要な場合、および利用目的の達成のために必要最小限の範囲内で発注者、委託先等の第三者に提供する場合を除き、事前の同意なしに第三者に個人情報を提供いたしません。</p>
              </li>
              <li class="p-csr--item">
                <h3 class="p-csr--item-ttl">3.業務委託の有無</h3>
                <p class="p-csr--item-des">当社は取得した個人情報の処理を外部の業者に委託することはありません。</p>
              </li>
              <li class="p-csr--item">
                <h3 class="p-csr--item-ttl">4.個人情報の開示・訂正・削除のご請求について</h3>
                <p class="p-csr--item-des">当社が保有するお客様情報に関して、ご本人様がご希望される場合には、お申し出いただいた方がご本人であることまたは正当な代理人であることを確認した上で、ご本人様の個人情報を開示し、ご指示により合理的な範囲内で内容の訂正・削除もいたします。<br />
                  開示等の対象となる開示対象個人情報項目<br />
                  ・氏名または名称<br />
                  ・住所<br />
                  ・電話番号<br />
                  ・メールアドレス<br />
                  ・生年月日<br />
                  ・性別<br />
                  なお、以下のいずれかに該当する場合は、開示などの対応はできません。<br />
                  ・ご本人または第三者の生命、身体、財産その他の権利利益を害するおそれがある場合<br />
                  ・当社の業務の適正な実施に著しい支障を及ぼすおそれがある場合<br />
                  ・法令に違反することとなる場合<br />
                  開示・訂正・削除につきましては、下記の相談窓口にご連絡下さい。
                </p>
              </li>
            </ul>
            <div class="p-csr--item-des p-privacy__box">
              <p><b>【個人情報に関する相談窓口】</b></p>
              <p>TEL：03-5335-5828<br />FAX：03-5347-0708<br />E-mail：info@a-planning-inc.co.jp</p>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
<div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>事業内容</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>