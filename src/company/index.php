<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="p-end p-company">
    <section class="p-end--banner">
      <div class="container">
        <h1 class="main-ttl">COMPANY<span>会社情報</span></h1>
      </div>
    </section><!-- ./p-recruit--banner -->
    <section class="p-company--cnt">
      <div class="container">
        <div class="p-company--ttlWrap type2">
          <div class="container">
            <h2 class="section-ttl2">会社概要</h2>
          </div>
        </div>
        <div class="tabs-navWrapper">
          <ul class="tabs-nav js-tabsNav">
            <li class="tabs-item js-tabsItem"><a href="/philosophy">ごあいさつ</a></li>
            <li class="tabs-item js-tabsItem active"><a href="/company">会社概要</a></li>
            <li class="tabs-item js-tabsItem"><a href="/csr">CSR方針</a></li>
          </ul>
        </div><!-- ./tabs-navWrapper -->
        <div class="p-infor">
          <ul class="p-infor--anchor">
            <li><a class="link" href="#profile">会社概要</a></li>
            <li><a class="link" href="#access">アクセス</a></li>
            <li><a class="link" href="#permission">許可・登録</a></li>
            <li><a class="link" href="#qualification">資格保有者一覧</a></li>
          </ul><!-- ./p-infor--anchor -->
          <div class="p-infor--qualifi">
            <h3 id="profile" class="section-ttl3">会社概要</h3>
            <div class="table type2 p-infor--qualifi-row">
              <table>
                <tr>
                  <th>会社名</th>
                  <td>株式会社エープランニング（英語表記：A-planning inc.,LTD）</td>
                </tr>
                <tr>
                  <th>代表者</th>
                  <td>代表取締役 原口 浩如</td>
                </tr>
                <tr>
                  <th>会社設立</th>
                  <td>1999年6月22日</td>
                </tr>
                <tr>
                  <th>本社所在地</th>
                  <td>〒167-0043 東京都杉並区上荻一丁目15番1号</td>
                </tr>
                <tr>
                  <th>電話番号 / FAX</th>
                  <td>TEL：03-5335-5828 / FAX：03-5347-0708</td>
                </tr>
                <tr>
                  <th>事業内容</th>
                  <td>
                    1. 各種一般測量
                    <br>2. 補償コンサルタント（全部門）業務
                    <br>3. 土地家屋調査測量業務
                    <br>4. 建設コンサルタント
                    <br>5. 環境調査
                    <br>6. 労働者派遣事業
                    <br>7. 有料職業紹介事業
                  </td>
                </tr>
                <tr>
                  <th>社員・役員数</th>
                  <td>245名（2021年3月現在）</td>
                </tr>
                <tr>
                  <th>資本金</th>
                  <td>16,000,000円</td>
                </tr>
                <tr>
                  <th>役員一覧</th>
                  <td>代表取締役 原口 浩如
                    <br>役員 〇〇 〇〇
                    <br>役員 〇〇 〇〇
                    <br>役員 〇〇 〇〇
                    <br>役員 〇〇 〇〇
                    <br>役員 〇〇 〇〇
                  </td>
                </tr>
              </table>
            </div>
            <div class="p-infor--qualifi-row">
              <h4 class="section-ttl5">組織図</h4>
              <div class="p-infor--qualifi-pdf">
                <div class="p-infor--qualifi-pdf-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/company/pdf.png" alt="">
                </div>
                <p class="p-infor--qualifi-pdf-des">2020年xx月xx日現在</p>
              </div>
            </div>
            <div class="p-infor--qualifi-row">
              <h3 class="section-ttl5">主要取引先</h3>
              <div class="table2 col25">
                <table>
                  <tr>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                  </tr>
                  <tr>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                  </tr>
                  <tr>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                  </tr>
                  <tr>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                  </tr>
                  <tr>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                  </tr>
                </table>
              </div>
            </div>
            <div class="p-infor--qualifi-row">
              <h3 class="section-ttl5">加盟団体</h3>
              <div class="table2">
                <table>
                  <tr>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                  </tr>
                  <tr>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                  </tr>
                  <tr>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                    <td>株式会社〇〇〇〇〇〇〇〇</td>
                  </tr>
                </table>
              </div>
            </div>
            <div class="p-infor--qualifi-row">
              <h3 id="access" class="section-ttl5">アクセス</h3>
              <div class="p-infor--qualifi-map">
                <div class="p-infor--qualifi-map-iframe">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3239.8585997300356!2d139.6162571152595!3d35.70509708018875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018edfa18770f67%3A0x2a22a0c98bbc086d!2z44CSMTY3LTAwNDMg5p2x5Lqs6YO95p2J5Lim5Yy65LiK6I2777yR5LiB55uu77yR77yV4oiS77yR!5e0!3m2!1sja!2sjp!4v1612250452066!5m2!1sja!2sjp" width="100%" height="auto" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
                <div class="p-infor--qualifi-map-guide">
                  <div class="p-infor--qualifi-map-infor">
                    <p class="p-infor--qualifi-map-ttl">本社所在地</p>
                    <p class="p-infor--qualifi-map-des">〒167-0043 <br>東京都杉並区上荻一丁目15番1号</p>
                  </div>
                  <div class="p-infor--qualifi-map-infor">
                    <p class="p-infor--qualifi-map-ttl">アクセス方法</p>
                    <p class="p-infor--qualifi-map-des">JR中央線・総武線「荻窪駅」 西口改札(三鷹方面)から徒歩1分<br>東京メトロ丸ノ内線「荻窪駅」 西出口から徒歩X分</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="p-infor--qualifi-row">
              <h3 id="permission" class="section-ttl5">許可・登録</h3>
              <div class="table type3">
                <table>
                  <tr>
                    <th>測量業者登録</th>
                    <td>登録番号：(4)第29774号<br>測量法第55条の5第1項の規定により登録</td>
                  </tr>
                  <tr>
                    <th>補償コンサルタント登録</th>
                    <td>登録番号：補29 第4601号 <br>補償コンサルタント登録規程第2条の規定により登録</td>
                  </tr>
                  <tr>
                    <th>一級建築士業務所登録</th>
                    <td>登録番号：一級 東京都知事登録 第58420号 <br>建築士法第23条の3第1項の規定に基づき登録</td>
                  </tr>
                  <tr>
                    <th>労働者派遣事業登録</th>
                    <td>登録番号：派13-302837 <br>法律第5条第1項の規定により登録</td>
                  </tr>
                  <tr>
                    <th>有料職業紹介事業登録</th>
                    <td>登録番号：13-ユ-306243</td>
                  </tr>
                </table>
              </div>
            </div>
            <div class="p-infor--qualifi-row">
              <h3 id="qualification" class="section-ttl5">資格保有者一覧</h3>
              <div class="table type3">
                <table>
                  <tr>
                    <th>測量士</th>
                    <td>60名</td>
                  </tr>
                  <tr>
                    <th>測量士補</th>
                    <td>39名</td>
                  </tr>
                  <tr>
                    <th>土地家屋調査士</th>
                    <td>26名</td>
                  </tr>
                  <tr>
                    <th>一級建築士</th>
                    <td>37名</td>
                  </tr>
                  <tr>
                    <th>二級建築士</th>
                    <td>34名</td>
                  </tr>
                  <tr>
                    <th>一・二級建築施工管理技士</th>
                    <td>36名</td>
                  </tr>
                  <tr>
                    <th>補償業務管理士</th>
                    <td>39名</td>
                  </tr>
                  <tr>
                    <th>一・二級土木施工管理技士</th>
                    <td>13名</td>
                  </tr>
                  <tr>
                    <th>宅地建物取引主任者</th>
                    <td>25名</td>
                  </tr>
                </table>
              </div>
              <h4 class="section-ttl6">補償業務管理士</h4>
              <div class="table type3">
                <table>
                  <tr>
                    <th>測量士</th>
                    <td>60名</td>
                  </tr>
                  <tr>
                    <th>測量士補</th>
                    <td>39名</td>
                  </tr>
                  <tr>
                    <th>土地家屋調査士</th>
                    <td>26名</td>
                  </tr>
                  <tr>
                    <th>一級建築士</th>
                    <td>37名</td>
                  </tr>
                </table>
              </div>
              <h4 class="section-ttl6">補償業務管理士</h4>
              <div class="table type3">
                <table>
                  <tr>
                    <th>測量士</th>
                    <td>60名</td>
                  </tr>
                  <tr>
                    <th>測量士補</th>
                    <td>39名</td>
                  </tr>
                  <tr>
                    <th>土地家屋調査士</th>
                    <td>26名</td>
                  </tr>
                  <tr>
                    <th>一級建築士</th>
                    <td>37名</td>
                  </tr>
                </table>
              </div>
              <p class="p-infor--qualifi-row-des">※複数の有資格者は再掲しております。</p>
            </div>
          </div>
        </div><!-- ./p-infor -->
      </div>
    </section>
  </div>
</main><!-- ./main -->
<div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>会社情報</li>
      <li>会社概要</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>