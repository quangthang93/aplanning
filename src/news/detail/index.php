<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="p-end p-news">
    <div class="p-end--banner">
      <div class="container">
        <h1 class="main-ttl">NEWS<span>ニュース</span></h1>
      </div>
    </div><!-- ./p-end--banner -->
    <div class="p-end--content">
      <div class="container">
        <h2 class="title-lv2">一覧に表示されるニュースのタイトルが入ります</h2>
        <div class="dateWrap">
          <span class="date">2019.00.00</span>
          <span class="tag type3">お知らせ</span>
        </div>
        <div class="no-reset">
          <img src="<?php echo $PATH;?>/assets/images/news/news01.png" alt="">
          <br>
          <br>
          <br>
          <h3>こちらに中見出しが入ります</h3>
          <br>
          <p>本文標準サイズです。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。</p>
          <br>
          <br>
          <p class="iframe">
            <iframe width="900" height="506" src="https://www.youtube.com/embed/6pxRHBw-k8M" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <!-- <img src="<?php echo $PATH;?>/assets/images/news/youtube.jpg" alt=""> -->
          </p>
          <br>
          <br>
          <h3>こちらに中見出しが入ります</h3>
          <h4>こちらに小見出しが入ります</h4>
          <p>これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。</p>
          <br>
          <ul>
            <li>リスト項目です</li>
            <li>リスト項目です</li>
            <li>リスト項目です</li>
            <li>リスト項目です</li>
          </ul>
          <br>
          <a href="" class="link-icon arrow">テキストリンク</a>
          <br>
          <a href="" class="link-icon blank">外部リンク</a>
          <br>
          <a href="" class="link-icon pdf">PDFリンク</a>
          <br>
          <br>
        </div><!-- ./p-news-content -->
        <a href="/news" class=" btn-view-more">ニュース一覧</a>
      </div>
    </div><!-- ./p-end--content -->
  </div>
</main><!-- ./main -->

<div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li><a href="/news">ニュース</a></li>
      <li>一覧に表示されるニュースのタイトルが入ります</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>