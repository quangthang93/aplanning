<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="p-end p-news">
    <div class="p-end--banner">
      <div class="container">
        <h1 class="main-ttl">NEWS<span>ニュース</span></h1>
      </div>
    </div><!-- ./p-end--banner -->
    <div class="p-end--content">
      <div class="container">
        <ul class="p-news--list">
          <li class="p-news--item">
            <div class="p-news--item-infor">
              <span class="date">2019.00.00</span>
              <span class="tag type3 sp-only2">採用情報</span>
              <p class="p-news--item-link"><a href="/news/detail" class="link-icon">中途採用募集(土地家屋調査士)のお知らせ</a></p>
            </div>
            <div class="p-news--item-tagWrap pc-only">
              <span class="tag type3">採用情報</span>
            </div>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-infor">
              <span class="date">2019.00.00</span>
              <span class="tag type3 sp-only2">採用情報</span>
              <p class="p-news--item-link"><a href="/news/detail" class="link-icon pdf">新型コロナウイルス感染症の拡大防止への対応状況について(土地家屋調査士)のお知らせ</a></p>
            </div>
            <div class="p-news--item-tagWrap pc-only">
              <!-- <span class="tag type3">お知らせ</span> -->
            </div>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-infor">
              <span class="date">2019.00.00</span>
              <!-- <span class="tag type3 sp-only2">採用情報</span> -->
              <p class="p-news--item-link"><a href="/news/detail" class="link-icon blank">中途採用募集(土地家屋調査士)のお知らせ</a></p>
            </div>
            <div class="p-news--item-tagWrap pc-only">
              <!-- <span class="tag type3">採用情報</span> -->
            </div>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-infor">
              <span class="date">2019.00.00</span>
              <span class="tag type3 sp-only2">お知らせ</span>
              <p class="p-news--item-link"><a href="/news/detail" class="link-icon">新型コロナウイルス感染症の拡大防止への対応状況について</a></p>
            </div>
            <div class="p-news--item-tagWrap pc-only">
              <span class="tag type3">お知らせ</span>
            </div>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-infor">
              <span class="date">2019.00.00</span>
              <span class="tag type3 sp-only2">重要なお知らせ</span>
              <p class="p-news--item-link"><a href="/news/detail" class="link-icon">新型コロナウイルス感染症の拡大防止への対応状況について</a></p>
            </div>
            <div class="p-news--item-tagWrap pc-only">
              <span class="tag type3">重要なお知らせ</span>
            </div>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-infor">
              <span class="date">2019.00.00</span>
              <span class="tag type3 sp-only2">セミナー・イベント</span>
              <p class="p-news--item-link"><a href="/news/detail" class="link-icon">新型コロナウイルス感染症の拡大防止への対応状況について</a></p>
            </div>
            <div class="p-news--item-tagWrap pc-only">
              <span class="tag type3">セミナー・イベント</span>
            </div>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-infor">
              <span class="date">2019.00.00</span>
              <span class="tag type3 sp-only2">採用情報</span>
              <p class="p-news--item-link"><a href="/news/detail" class="link-icon">新型コロナウイルス感染症の拡大防止への対応状況について</a></p>
            </div>
            <div class="p-news--item-tagWrap pc-only">
              <span class="tag type3">採用情報</span>
            </div>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-infor">
              <span class="date">2019.00.00</span>
              <span class="tag type3 sp-only2">採用情報</span>
              <p class="p-news--item-link"><a href="/news/detail" class="link-icon">新型コロナウイルス感染症の拡大防止への対応状況について</a></p>
            </div>
            <div class="p-news--item-tagWrap pc-only">
              <span class="tag type3">採用情報</span>
            </div>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-infor">
              <span class="date">2019.00.00</span>
              <span class="tag type3 sp-only2">採用情報</span>
              <p class="p-news--item-link"><a href="/news/detail" class="link-icon">新型コロナウイルス感染症の拡大防止への対応状況について</a></p>
            </div>
            <div class="p-news--item-tagWrap pc-only">
              <span class="tag type3">採用情報</span>
            </div>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-infor">
              <span class="date">2019.00.00</span>
              <span class="tag type3 sp-only2">採用情報</span>
              <p class="p-news--item-link"><a href="/news/detail" class="link-icon">新型コロナウイルス感染症の拡大防止への対応状況について</a></p>
            </div>
            <div class="p-news--item-tagWrap pc-only">
              <span class="tag type3">採用情報</span>
            </div>
          </li>
        </ul>
        <div class="pagination">
          <div class="pagination-list">
            <a class="ctrl prev" href="">
              <svg xmlns="http://www.w3.org/2000/svg" width="6.121" height="9.414" viewBox="0 0 6.121 9.414">
                <g id="_1" data-name=" 1" transform="translate(1267.414 519.938) rotate(180)">
                  <path class="a" id="_1" data-name="&gt;" d="M814.332,1939.086l4,4-4,4" transform="translate(447.668 -1427.855)" fill="none" stroke="#ad002d" stroke-width="2" />
                </g>
              </svg>
            </a>
            <a href="">1</a>
            <a href="">2</a>
            <a class="active" href="">3</a>
            <a href="">4</a>
            <a href="">5</a>
            <a class="ctrl next" href="">
              <svg xmlns="http://www.w3.org/2000/svg" width="6.121" height="9.414" viewBox="0 0 6.121 9.414">
                <g id="シンボル_82" data-name="シンボル 82" transform="translate(-1259.293 -510.062)">
                  <path class="a" id="_" data-name="&gt;" d="M814.332,1939.086l4,4-4,4" transform="translate(445.668 -1428.316)" fill="none" stroke="#ad002d" stroke-width="2" />
                </g>
              </svg>
            </a>
          </div>
        </div>
      </div>
    </div><!-- ./p-end--content -->
  </div>
</main><!-- ./main -->

<div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>ニュース</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>