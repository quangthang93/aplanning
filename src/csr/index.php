<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="p-end p-company">
    <section class="p-end--banner">
      <div class="container">
        <h1 class="main-ttl">COMPANY<span>会社情報</span></h1>
      </div>
    </section><!-- ./p-recruit--banner -->
    <section class="p-company--cnt">
      <div class="container">
        <div class="p-company--ttlWrap type2">
          <div class="container">
            <h2 class="section-ttl2">CSR方針</h2>
          </div>
        </div>
        <div class="tabs-navWrapper">
          <ul class="tabs-nav js-tabsNav">
            <li class="tabs-item js-tabsItem"><a href="/philosophy">ごあいさつ</a></li>
            <li class="tabs-item js-tabsItem"><a href="/company">会社概要</a></li>
            <li class="tabs-item js-tabsItem active"><a href="/csr">CSR方針</a></li>
          </ul>
        </div><!-- ./tabs-navWrapper -->
        <div class="p-csr">
          <div class="title-lv2">社会から信頼される企業であり続けることを目指して</div>
          <p class="desc4">株式会社エープランニングは、創業以来、公共事業に従事してまいりました。「技術力」と「誠意」をもって社会に貢献するという創業精神のもと、あらゆるステークホルダーから「信頼」される企業を目指し、高い社会的倫理観をもちながら、ＣＳＲの一環として、積極的に社会貢献に寄与する活動に取り組み続け、社会から信頼される企業であり続けたいと考えています。</p>
          <ul class="p-csr--list">
            <li class="p-csr--item">
              <h3 class="p-csr--item-ttl">1.企業活動としての社会的責任の自覚</h3>
              <p class="p-csr--item-des">役員および社員は、ＣＳＲが企業活動そのものであることを自覚し、社会および事業の持続的発展を図るべく、本取り組み方針に基づいて、社会的責任を果たしていきます。</p>
            </li>
            <li class="p-csr--item">
              <h3 class="p-csr--item-ttl">2.事業活動を通じた社会への貢献</h3>
              <p class="p-csr--item-des">優れた技術力を基盤とした事業活動によって、安全・安心な国土づくりを実現し、豊かで活力のある社会の構築に貢献します。</p>
            </li>
            <li class="p-csr--item">
              <h3 class="p-csr--item-ttl">3.企業倫理と人権の尊重</h3>
              <p class="p-csr--item-des">基本的人権を尊重し、個人の尊厳を大切にするとともに、働き甲斐のある明るい企業風土の醸成に努めます。</p>
            </li>
            <li class="p-csr--item">
              <h3 class="p-csr--item-ttl">4.環境保全の推進</h3>
              <p class="p-csr--item-des">地球環境の永続的保護が人類共通の重要課題であることを認識し、自らの社会的責任を果たすために、地球環境に配慮した事業活動を通じ、環境負荷の低減に取り組みます。</p>
            </li>
            <li class="p-csr--item">
              <h3 class="p-csr--item-ttl">5.社会貢献の推進</h3>
              <p class="p-csr--item-des">企業市民として職能を活かした社会貢献活動を積極的に行い、安心で豊かな社会の実現に貢献します。</p>
            </li>
            <li class="p-csr--item">
              <h3 class="p-csr--item-ttl">6.働きやすい職場作り</h3>
              <p class="p-csr--item-des">すべての社員にとって、働きやすい、達成感のある職場作りに努めると共に、仕事を通じた自己実現や自己成長を図ることのできる、意欲ある社員を積極的に支援します。</p>
            </li>
            <li class="p-csr--item">
              <h3 class="p-csr--item-ttl">7.社会的責任意識の共有化</h3>
              <p class="p-csr--item-des">すべての取引先に協力を求めて、社会的責任意識を共有化し、公正、かつ健全な事業活動の推進に努めます。</p>
            </li>
            <li class="p-csr--item">
              <h3 class="p-csr--item-ttl">8.安全衛生の推進</h3>
              <p class="p-csr--item-des">社員一人ひとりが心身ともに健康で元気に、そして働きがいとやりがいを持って仕事に取り組める環境こそ成長の基盤ととらえ、「個人の健康」と「組織の健康」を同時に追求するという健康経営に注力しています。</p>
            </li>
          </ul>
        </div><!-- ./p-csr -->
      </div>
    </section>
  </div>
</main><!-- ./main -->
<div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>会社情報</li>
      <li>CSR方針</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>