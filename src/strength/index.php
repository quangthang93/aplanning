<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="p-end">
    <div class="p-end--banner">
      <div class="container">
        <h1 class="main-ttl">ABOUT<span>当社の強み</span></h1>
      </div>
    </div><!-- ./p-recruit--banner -->
    <div class="p-strength">
      <div class="p-strength--reason">
        <div class="p-strength--reason-cnt">
          <div class="p-strength--reason-bg">
            <img src="<?php echo $PATH;?>/assets/images/strength/bg-reason01.png" alt="">
            <img src="<?php echo $PATH;?>/assets/images/strength/bg-reason02.png" alt="">
          </div>
          <div class="p-strength--reason-intro">
            <div class="container">
              <h2 class="p-strength--reason-intro-ttl">測量・調査からコンサルタントまで<br class="pc-only3">まちづくりをトータルサポート</h2>
              <p class="p-strength--reason-intro-summary">
                未来を見据えた、豊かなまちづくりには、高度な測量技術による地域の特長と課題を把握することが重要です。<br><br>エープランニングは、お客様のパートナーシップカンパニーとして、高速道路の建設や区画整理など、公共事業に伴う測量・調査から補償・建設のコンサルタント業務、技術者の人材派遣まで、トータルサポートするサービスをご提供します。こちらは仮テキストです。
              </p>
            </div>
          </div><!-- ./p-strength--reason-intro -->
          <div class="p-strength--reason-list">
            <div class="p-strength--reason-row">
              <div class="p-strength--reason-row-thumb">
                <img src="<?php echo $PATH;?>/assets/images/strength/reason01.png" alt="">
              </div>
              <div class="p-strength--reason-row-cnt">
                <p class="p-strength--reason-row-label">REASON 01</p>
                <p class="p-strength--reason-row-ttl">多彩で豊富な実績と確かな技術力</p>
                <p class="p-strength--reason-row-des">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。</p>
              </div>
            </div><!-- ./p-strength--reason-row -->
            <div class="p-strength--reason-row">
              <div class="p-strength--reason-row-thumb">
                <img src="<?php echo $PATH;?>/assets/images/strength/reason02.png" alt="">
              </div>
              <div class="p-strength--reason-row-cnt reason02">
                <p class="p-strength--reason-row-label">REASON 02</p>
                <p class="p-strength--reason-row-ttl">様々なニーズに対応できる広い事業領域</p>
                <p class="p-strength--reason-row-des">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。</p>
              </div>
            </div><!-- ./p-strength--reason-row -->
            <div class="p-strength--reason-row">
              <div class="p-strength--reason-row-thumb">
                <img src="<?php echo $PATH;?>/assets/images/strength/reason03.png" alt="">
              </div>
              <div class="p-strength--reason-row-cnt reason03">
                <p class="p-strength--reason-row-label">REASON 03</p>
                <p class="p-strength--reason-row-ttl">独自の人材育成プログラム</p>
                <p class="p-strength--reason-row-des">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。</p>
              </div>
            </div><!-- ./p-strength--reason-row -->
          </div><!-- ./p-strength--reason-list -->
        </div><!-- ./p-strength--reason-cnt -->
      </div><!-- ./p-strength--reason -->
      <div class="p-strength--job">
        <div class="container">
          <h2 class="title-lv2">事業内容</h2>
          <ul class="p-strength--job-list">
            <li class="p-strength--job-item">
              <a class="link" href="">
                <div class="p-strength--job-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/strength/job01.png" alt="">
                </div>
                <h3 class="title-lv4">各種一般測量</h3>
              </a>
            </li>
            <li class="p-strength--job-item">
              <a class="link" href="">
                <div class="p-strength--job-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/strength/job02.png" alt="">
                </div>
                <h3 class="title-lv4">補償コンサルタント(全部門)業務</h3>
              </a>
            </li>
            <li class="p-strength--job-item">
              <a class="link" href="">
                <div class="p-strength--job-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/strength/job03.png" alt="">
                </div>
                <h3 class="title-lv4">土地家屋調査測量業務</h3>
              </a>
            </li>
            <li class="p-strength--job-item">
              <a class="link" href="">
                <div class="p-strength--job-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/strength/job04.png" alt="">
                </div>
                <h3 class="title-lv4">建設コンサルタント</h3>
              </a>
            </li>
            <li class="p-strength--job-item">
              <a class="link" href="">
                <div class="p-strength--job-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/strength/job05.png" alt="">
                </div>
                <h3 class="title-lv4">環境調査</h3>
              </a>
            </li>
            <li class="p-strength--job-item">
              <a class="link" href="">
                <div class="p-strength--job-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/strength/job06.png" alt="">
                </div>
                <h3 class="title-lv4">一般労働者派遣事業</h3>
              </a>
            </li>
            <li class="p-strength--job-item">
              <a class="link" href="">
                <div class="p-strength--job-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/strength/job07.png" alt="">
                </div>
                <h3 class="title-lv4">有料職業紹介事業</h3>
              </a>
            </li>
          </ul>
          <div class="view-moreWrap"><a href="" class="view-more type2">事業内容一覧を見る</a></div>
        </div>
      </div><!-- ./p-strength--job -->
    </div><!-- ./p-strength -->
  </div>
</main><!-- ./main -->

<div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>当社の強み</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>