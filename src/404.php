<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="p-end p-business">
    <div class="p-end--banner">
      <div class="container">
        <h1 class="main-ttl">404 NOT FOUND</h1>
      </div>
    </div><!-- ./p-recruit--banner -->
    <div class="p-business--cnt">
      <div class="container">
        <p class="p-business--intro">申し訳ございません。お探しのページは見つかりませんでした。<br>
		  お客様がアクセスしようとしたページは、 掲載期間が終了し削除されたか、別の場所に移動した可能性があります。
メニューから引き続き当社ホームページをご覧くださいませ。</p>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>404 NOT FOUND</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>