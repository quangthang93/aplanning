<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="p-end p-recruit">
    <div class="p-end--banner">
      <div class="container">
        <h1 class="main-ttl">RECRUIT<span>採用情報</span></h1>
      </div>
    </div><!-- ./p-recruit--banner -->
    <div class="p-recruit--message fadeup2">
      <p class="p-recruit--message-cnt">未来を見据えた<br>持続可能な<br>まちづくりを実現する</p>
    </div><!-- ./p-recruit--banner -->
    <p class="p-recruit--intro fadeup2">エープランニングは、公共事業に伴う測量・調査から補償・建設のコンサルタント業務まで、まちづくりにかかわるサービスを提供しています。安全で住みよいまちとくらしを実現するために、当社の一員として活躍いただける仲間を募集しています。</p>
    <section class="p-recruit--business fadeup">
      <div class="container">
        <h2 class="section-ttl">BUSINESS<span>仕事を知る</span></h2>
      </div>
      <div class="p-recruit--business-cnt">
        <p class="p-recruit--business-summary">当社は、公共事業に伴う測量・調査から補償・建設のコンサルタント業務まで、まちづくりにかかわるサービスを提供しています。</p>
        <ul class="p-recruit--business-list">
          <li class="p-recruit--business-item">
            <div class="p-recruit--business-item-link">
              <h3 class="p-recruit--business-item-ttl">測量・調査業務</h3>
              <p class="p-recruit--business-item-des">道路・河川などの測量・調査および関連業務を行っています。お客様のご要望にあった、精度の高いデータの提供に努めています。​</p>
            </div>
          </li>
          <li class="p-recruit--business-item">
            <div class="p-recruit--business-item-link item02">
              <h3 class="p-recruit--business-item-ttl">補償コンサルタント業務</h3>
              <p class="p-recruit--business-item-des">公共事業の施行に伴う土地の調査・評価等、関係者の補償に関するコンサルティング業務を行います。こちらは仮テキストです。</p>
            </div>
          </li>
          <li class="p-recruit--business-item">
            <div class="p-recruit--business-item-link item03">
              <h3 class="p-recruit--business-item-ttl">建設コンサルタント業務 </h3>
              <p class="p-recruit--business-item-des">高速道路の建設や区画整理事業の施工に伴うコンサルティング業務を行います。こちらは仮テキストです。</p>
            </div>
          </li>
        </ul>
        <p class="p-recruit--business-more"><a class="view-more2" href="">READ MORE</a></p>
      </div>
    </section><!-- ./p-recruit--business -->
    <section class="p-recruit--action fadeup">
      <img class="p-recruit--action-img01" src="<?php echo $PATH;?>/assets/images/recruit/action01.png" alt="">
      <img class="p-recruit--action-img02" src="<?php echo $PATH;?>/assets/images/recruit/action02.png" alt="">
      <img class="p-recruit--action-img03" src="<?php echo $PATH;?>/assets/images/recruit/action03.png" alt="">
      <div class="p-recruit--action-cnt">
        <h2 class="section-ttl">POSITIVE ACTION<span>求める人材像</span></h2>
        <ul class="p-recruit--action-list">
          <li class="p-recruit--action-item">
            <span class="p-recruit--action-item-icon">
              <img src="<?php echo $PATH;?>/assets/images/recruit/icon-people.svg" alt="">
            </span>
            <div class="p-recruit--action-item-cnt">
              <h4 class="p-recruit--action-item-ttl">責任感と判断力を併せもつ方</h4>
              <p class="p-recruit--action-item-des">まちづくりの仕事には、常に正確さが求められます。測量・調査における数字上の正確さだけではなく、地域社会の特徴や未来を考慮した判断力が必要です。</p>
            </div>
          </li>
          <li class="p-recruit--action-item">
            <span class="p-recruit--action-item-icon">
              <img src="<?php echo $PATH;?>/assets/images/recruit/icon-people.svg" alt="">
            </span>
            <div class="p-recruit--action-item-cnt">
              <h4 class="p-recruit--action-item-ttl">コミュニケーション能力の高い方</h4>
              <p class="p-recruit--action-item-des">こちらは仮テキストです。こちらは仮テキストです。こちらは仮テキストです。こちらは仮テキストです。こちらは仮テキストです。こちらは仮テキストです。</p>
            </div>
          </li>
          <li class="p-recruit--action-item">
            <span class="p-recruit--action-item-icon">
              <img src="<?php echo $PATH;?>/assets/images/recruit/icon-people.svg" alt="">
            </span>
            <div class="p-recruit--action-item-cnt">
              <h4 class="p-recruit--action-item-ttl">スキルアップに積極的な方</h4>
              <p class="p-recruit--action-item-des">こちらは仮テキストです。こちらは仮テキストです。こちらは仮テキストです。こちらは仮テキストです。こちらは仮テキストです。こちらは仮テキストです。</p>
            </div>
          </li>
        </ul>
      </div>
    </section><!-- ./p-recruit--action -->
    <section class="p-recruit--people fadeup">
      <div class="container">
        <h2 class="section-ttl" id="ourpeople">OUR PEOPLE<span>人を知る</span></h2>
      </div>
      <ul class="p-recruit--people-list">
        <li class="p-recruit--people-item">
          <div class="container">
            <div class="p-recruit--people-item-top">
              <span class="p-recruit--people-item-icon">
                <img src="<?php echo $PATH;?>/assets/images/recruit/people01.svg" alt="">
              </span>
              <div class="p-recruit--people-item-infor">
                <p class="p-recruit--people-item-summary">専門性の技術で財産の価値を守り、見つめる</p>
                <div class="p-recruit--people-item-nameWrap">
                  <div class="p-recruit--people-item-name">
                    <h5 class="p_name">TAROU<br>YAMADA</h5>
                    <p class="p_year"><span class="p_year-label">部署名</span><br>2000年入社</p>
                  </div>
                  <div class="p-recruit--people-item-des pc-only">
                    <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                    <p>の文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="p-recruit--people-item-des sp-only">
              <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
              <p>の文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
            </div>
          </div>
        </li>
        <li class="p-recruit--people-item">
          <div class="container">
            <div class="p-recruit--people-item-top">
              <span class="p-recruit--people-item-icon">
                <img src="<?php echo $PATH;?>/assets/images/recruit/people02.svg" alt="">
              </span>
              <div class="p-recruit--people-item-infor">
                <p class="p-recruit--people-item-summary">人々の暮らしを支え続け、地域の未来に貢献できる</p>
                <div class="p-recruit--people-item-nameWrap">
                  <div class="p-recruit--people-item-name">
                    <h5 class="p_name">YOSUKE<br>TANAKA</h5>
                    <p class="p_year"><span class="p_year-label">部署名</span><br>2000年入社</p>
                  </div>
                  <div class="p-recruit--people-item-des pc-only">
                    <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                    <p>の文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="p-recruit--people-item-des sp-only">
              <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
              <p>の文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
            </div>
          </div>
        </li>
        <li class="p-recruit--people-item">
          <div class="container">
            <div class="p-recruit--people-item-top">
              <span class="p-recruit--people-item-icon">
                <img src="<?php echo $PATH;?>/assets/images/recruit/people03.svg" alt="">
              </span>
              <div class="p-recruit--people-item-infor">
                <p class="p-recruit--people-item-summary">仕事を通じて自分の専門性を高めて日々成長する</p>
                <div class="p-recruit--people-item-nameWrap">
                  <div class="p-recruit--people-item-name">
                    <h5 class="p_name">TAROU<br>YAMADA</h5>
                    <p class="p_year"><span class="p_year-label">部署名</span><br>2000年入社</p>
                  </div>
                  <div class="p-recruit--people-item-des pc-only">
                    <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                    <p>の文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="p-recruit--people-item-des sp-only">
              <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
              <p>の文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
            </div>
          </div>
        </li>
        <li class="p-recruit--people-item more js-peopleMore">
          <div class="container">
            <div class="p-recruit--people-item-top">
              <span class="p-recruit--people-item-icon">
                <img src="<?php echo $PATH;?>/assets/images/recruit/people04.svg" alt="">
              </span>
              <div class="p-recruit--people-item-infor">
                <p class="p-recruit--people-item-summary">専門性の技術で財産の価値を守り、見つめる</p>
                <div class="p-recruit--people-item-nameWrap">
                  <div class="p-recruit--people-item-name">
                    <h5 class="p_name">TAROU<br>YAMADA</h5>
                    <p class="p_year"><span class="p_year-label">部署名</span><br>2000年入社</p>
                  </div>
                  <div class="p-recruit--people-item-des pc-only">
                    <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                    <p>の文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="p-recruit--people-item-des sp-only">
              <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
              <p>の文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
            </div>
          </div>
        </li>
        <li class="p-recruit--people-item more js-peopleMore">
          <div class="container">
            <div class="p-recruit--people-item-top">
              <span class="p-recruit--people-item-icon">
                <img src="<?php echo $PATH;?>/assets/images/recruit/people05.svg" alt="">
              </span>
              <div class="p-recruit--people-item-infor">
                <p class="p-recruit--people-item-summary">人々の暮らしを支え続け、地域の未来に貢献できる</p>
                <div class="p-recruit--people-item-nameWrap">
                  <div class="p-recruit--people-item-name">
                    <h5 class="p_name">YOSUKE<br>TANAKA</h5>
                    <p class="p_year"><span class="p_year-label">部署名</span><br>2000年入社</p>
                  </div>
                  <div class="p-recruit--people-item-des pc-only">
                    <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                    <p>の文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="p-recruit--people-item-des sp-only">
              <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
              <p>の文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
            </div>
          </div>
        </li>
        <li class="p-recruit--people-item more js-peopleMore">
          <div class="container">
            <div class="p-recruit--people-item-top">
              <span class="p-recruit--people-item-icon">
                <img src="<?php echo $PATH;?>/assets/images/recruit/people06.svg" alt="">
              </span>
              <div class="p-recruit--people-item-infor">
                <p class="p-recruit--people-item-summary">仕事を通じて自分の専門性を高めて日々成長する</p>
                <div class="p-recruit--people-item-nameWrap">
                  <div class="p-recruit--people-item-name">
                    <h5 class="p_name">TAROU<br>YAMADA</h5>
                    <p class="p_year"><span class="p_year-label">部署名</span><br>2000年入社</p>
                  </div>
                  <div class="p-recruit--people-item-des pc-only">
                    <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                    <p>の文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="p-recruit--people-item-des sp-only">
              <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
              <p>の文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
            </div>
          </div>
        </li>
        <li class="p-recruit--people-item more js-peopleMore">
          <div class="container">
            <div class="p-recruit--people-item-top">
              <span class="p-recruit--people-item-icon">
                <img src="<?php echo $PATH;?>/assets/images/recruit/people07.svg" alt="">
              </span>
              <div class="p-recruit--people-item-infor">
                <p class="p-recruit--people-item-summary">専門性の技術で財産の価値を守り、見つめる</p>
                <div class="p-recruit--people-item-nameWrap">
                  <div class="p-recruit--people-item-name">
                    <h5 class="p_name">TAROU<br>YAMADA</h5>
                    <p class="p_year"><span class="p_year-label">部署名</span><br>2000年入社</p>
                  </div>
                  <div class="p-recruit--people-item-des pc-only">
                    <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                    <p>の文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="p-recruit--people-item-des sp-only">
              <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
              <p>の文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
            </div>
          </div>
        </li>
      </ul><!-- ./p-recruit--people-list -->
      <div class="container align-center">
        <a href="javascript:void(0)" class="btn-view-more3 js-btnPeopleMore">ALL PEOPLE</a>
        <a href="javascript:void(0)" class="btn-view-more6 js-btnPeopleClose">CLOSE</a>
      </div>
    </section><!-- ./p-recruit--action -->
    <section class="p-recruit--job fadeup">
      <div class="container">
        <h2 class="section-ttl" id="joblist">JOB LIST<span>募集要項</span></h2>
        <div class="p-recruit--job-cnt">
          <div class="p-recruit--job-notice">
            <p class="p-recruit--job-notice-ttl">下記の資格をお持ちの方は随時募集中です。</p>
            <ul class="p-recruit--job-notice-list">
              <li>測量士補・測量士</li>
              <li>土地家屋調査士</li>
              <li>補償業務管理士</li>
              <li>一級建築士</li>
              <li>二級建築士</li>
              <li>建築施工管理技士</li>
            </ul>
          </div>
          <ul class="p-recruit--job-directs">
            <li>
              <a href="/recruit/detail/#job01" class="btn-view-more4">一級建築施工管理技士</a>
            </li>
            <li>
              <a href="/recruit/detail/#job02" class="btn-view-more4">測量・建築・補償業務</a>
            </li>
            <li>
              <a href="/recruit/detail/#job03" class="btn-view-more4">第一級陸上特殊無線技士<br class="sp-only">(資格必須)</a>
            </li>
            <li>
              <a href="/recruit/detail/#job04" class="btn-view-more4">総務事務補助</a>
            </li>
          </ul>
        </div>
      </div>
    </section><!-- ./p-recruit--job -->
    <section class="p-recruit--graphics">
      <div class="container">
        <div class="p-recruit--graphics-list">
          <div class="p-recruit--graphics-item txt">
            <p class="p-recruit--graphics-item-ttl">INFO<br>GRAPHICS</p>
            <p class="p-recruit--graphics-item-desc">数字で見るエープランニング</p>
          </div>
          <div class="p-recruit--graphics-item img">
            <img src="<?php echo $PATH;?>/assets/images/recruit/graphics.svg" alt="">
          </div>
        </div>
        <div class="align-center"><a href="/data" class="p-recruit--graphics-more">READ MORE</a></div>
      </div>
    </section><!-- ./p-recruit--graphics -->
    <section class="p-recruit--entry fadeup">
      <div class="container">
        <h2 class="section-ttl">ENTRY<span>応募方法</span></h2>
        <p class="p-recruit--entry-summary">当社に興味をお持ちの方は、下記の連絡先よりお問い合わせください。折り返し、採用担当者からご連絡します。</p>
        <div class="p-recruit--entry-inforWrap">
          <div class="p-recruit--entry-infor">
            <p class="p-recruit--entry-infor-ttl">採用に関するお問い合わせ先</p>
            <p class="p-recruit--entry-infor-des">株式会社エープランニング　〇〇部 採用担当(担当:大熊)<br>〒 167-0043　東京都杉並区上荻一丁目15番1号<br>TEL：03-5335-5828<br>E-Mail：info@a-planning-inc.co.jp</p>
          </div>
          <div class="p-recruit--entry-direct">
            <a href="" class="btn-view-more5">ENTRY<span>エントリー</span></a>
          </div>
        </div>
      </div>
    </section>
  </div>
</main><!-- ./main -->

<div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>採用情報</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>