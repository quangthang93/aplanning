<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="p-end p-recruit">
    <div class="p-end--banner">
      <div class="container">
        <h1 class="main-ttl">RECRUIT<span>採用情報</span></h1>
      </div>
    </div><!-- ./p-recruit--banner -->
    <div class="p-requirements">
      <div class="container">
        <h2 class="section-ttl2">募集要項</h2>
        <ul class="p-requirements--anchor">
          <li><a class="link" href="#jobTitle01">土地家屋調査士</a></li>
          <li><a class="link" href="#jobTitle02">一級建築施工管理技士</a></li>
          <li><a class="link" href="#jobTitle03">募集職種名</a></li>
          <li><a class="link" href="#jobTitle04">募集職種名</a></li>
          <li><a class="link" href="#jobTitle05">募集職種名</a></li>
          <li><a class="link" href="#jobTitle06">募集職種名</a></li>
        </ul><!-- ./p-requirements--anchor -->    
        <div class="p-requirements--row" id="job01">
          <h3 class="section-ttl3" id="jobTitle01">土地家屋調査士</h3>
          <div class="p-requirements--table">
            <table class="table">
              <tr>
                <th>仕事内容</th>
                <td>高速道路の建設、区画整理事業の施工など公共事業を多数受注しているた めご経験に合わせ案件に配属予定。社員の労働環境に気を遣い、無理な案件は受けない方針のため月の残業時間は15時間程。<br> 一流技術者達が腰を据えて長期就業しています。 公共用地測量並びに成果品図面等の検収、地権者対応、その他付随するPC操作等を行って頂きます。案件次第ですが工期は半年～7、8年まで、勤務地はと東北～沖縄まで(関東圏・関西圏が主)で、その都度社員の希望を加味しながらアサインしていきます。<br> [例] 数年間のプロジェクトの場合、私情で一定期間しか担当できないなど調整可能。</td>
              </tr>
              <tr>
                <th>必要な経験・能力等</th>
                <td>[必須] 土地家屋調査士<br> [歓迎] 公共工事に関わる経験がある方</td>
              </tr>
              <tr>
                <th>給与</th>
                <td>月給: 220,000～450,000</td>
              </tr>
              <tr>
                <th>対象</th>
                <td>中途、新卒</td>
              </tr>
              <tr>
                <th>雇用契約</th>
                <td>正社員<br>期間の定め: 無し</td>
              </tr>
              <tr>
                <th>賞与</th>
                <td>年2回</td>
              </tr>
              <tr>
                <th>勤務時間</th>
                <td>8:00～16:25 (所定労働時間: 7時間40分)</td>
              </tr>
              <tr>
                <th>勤務地</th>
                <td>本社: 東京メトロ丸ノ内線「荻窪」駅 徒歩3分<br> ※勤務地は各現場となります。敷地内禁煙(屋内喫煙可能場所あり)<br>転勤有</td>
              </tr>
              <tr>
                <th>休日</th>
                <td>124日 (土日祝、夏季休暇3日、年末年始6日)<br>有給休暇: 10～20日 (全労働日の8割以上出勤した場合)</td>
              </tr>
              <tr>
                <th>各種保険</th>
                <td>健康保険、厚生年金保険、雇用保険、労災保険</td>
              </tr>
              <tr>
                <th>制度・福利厚生</th>
                <td>家族手当: 10,000円～<br>職責手当: 2,000円～</td>
              </tr>
            </table>
          </div><!-- ./p-requirements--table -->    
          <div class="p-requirements--entry">
            <a href="" class="btn-view-more5">ENTRY<span>エントリー</span></a>
          </div>
        </div><!-- ./p-requirements--row --> 
        <div class="p-requirements--row" id="job02">
          <h3 class="section-ttl3" id="jobTitle02">一級建築施工管理技士</h3>
          <div class="p-requirements--table">
            <table class="table">
              <tr>
                <th>仕事内容</th>
                <td>一級建築施工管理技士資格を活かし公共事業の現場でご活躍いただきます。建築職として管理施設の新築・増改築工事における、発注者支援業務の施工計画・工程・安全・品質・予算管理の補助を行っていただきます。資格を活かした業務を担当いただきますが、詳細は各現場によって異なるため、面接にてお伺いいただければと思います。 <br>[例]高速道路建設、区画整理事業の施工など <br>工期も案件によって異なり、半年～長いと7,8年と様々です。将来的にはメインでCM業務を担当いただきます。</td>
              </tr>
              <tr>
                <th>必要な経験・能力等</th>
                <td>[必須] 一級建築施工管理技士資格<br>[歓迎] 公共工事に関わる経験がある方</td>
              </tr>
              <tr>
                <th>給与</th>
                <td>月給: 330,000～500,000</td>
              </tr>
              <tr>
                <th>対象</th>
                <td>中途、新卒</td>
              </tr>
              <tr>
                <th>雇用契約</th>
                <td>正社員<br>期間の定め: 無し</td>
              </tr>
              <tr>
                <th>賞与</th>
                <td>年2回</td>
              </tr>
              <tr>
                <th>勤務時間</th>
                <td>8:00～16:25 (所定労働時間: 7時間40分)</td>
              </tr>
              <tr>
                <th>勤務地</th>
                <td>本社: 東京メトロ丸ノ内線「荻窪」駅 徒歩3分<br>※勤務地は各現場となります。敷地内禁煙(屋内喫煙可能場所あり)<br> 転勤有</td>
              </tr>
              <tr>
                <th>休日</th>
                <td>124日 (土日祝、夏季休暇3日、年末年始6日)<br>有給休暇: 10～20日 (全労働日の8割以上出勤した場合)</td>
              </tr>
              <tr>
                <th>各種保険</th>
                <td>健康保険、厚生年金保険、雇用保険、労災保険</td>
              </tr>
              <tr>
                <th>制度・福利厚生</th>
                <td>家族手当: 10,000円～<br>職責手当: 2,000円～</td>
              </tr>
            </table>
          </div><!-- ./p-requirements--table -->    
          <div class="p-requirements--entry">
            <a href="" class="btn-view-more5">ENTRY<span>エントリー</span></a>
          </div>
        </div><!-- ./p-requirements--row -->
        <div class="p-requirements--row" id="job03">
          <h3 class="section-ttl3" id="jobTitle03">募集職種名</h3>
          <div class="p-requirements--table">
            <table class="table">
              <tr>
                <th>仕事内容</th>
                <td>一級建築施工管理技士資格を活かし公共事業の現場でご活躍いただきます。建築職として管理施設の新築・増改築工事における、発注者支援業務の施工計画・工程・安全・品質・予算管理の補助を行っていただきます。資格を活かした業務を担当いただきますが、詳細は各現場によって異なるため、面接にてお伺いいただければと思います。 <br>[例]高速道路建設、区画整理事業の施工など <br>工期も案件によって異なり、半年～長いと7,8年と様々です。将来的にはメインでCM業務を担当いただきます。</td>
              </tr>
              <tr>
                <th>必要な経験・能力等</th>
                <td>[必須] 一級建築施工管理技士資格<br>[歓迎] 公共工事に関わる経験がある方</td>
              </tr>
              <tr>
                <th>給与</th>
                <td>月給: 330,000～500,000</td>
              </tr>
              <tr>
                <th>対象</th>
                <td>中途、新卒</td>
              </tr>
              <tr>
                <th>雇用契約</th>
                <td>正社員<br>期間の定め: 無し</td>
              </tr>
              <tr>
                <th>賞与</th>
                <td>年2回</td>
              </tr>
              <tr>
                <th>勤務時間</th>
                <td>8:00～16:25 (所定労働時間: 7時間40分)</td>
              </tr>
              <tr>
                <th>勤務地</th>
                <td>本社: 東京メトロ丸ノ内線「荻窪」駅 徒歩3分<br>※勤務地は各現場となります。敷地内禁煙(屋内喫煙可能場所あり)<br> 転勤有</td>
              </tr>
              <tr>
                <th>休日</th>
                <td>124日 (土日祝、夏季休暇3日、年末年始6日)<br>有給休暇: 10～20日 (全労働日の8割以上出勤した場合)</td>
              </tr>
              <tr>
                <th>各種保険</th>
                <td>健康保険、厚生年金保険、雇用保険、労災保険</td>
              </tr>
              <tr>
                <th>制度・福利厚生</th>
                <td>家族手当: 10,000円～<br>職責手当: 2,000円～</td>
              </tr>
            </table>
          </div><!-- ./p-requirements--table -->    
          <div class="p-requirements--entry">
            <a href="" class="btn-view-more5">ENTRY<span>エントリー</span></a>
          </div>
        </div><!-- ./p-requirements--row -->
        <div class="p-requirements--row" id="job04">
          <h3 class="section-ttl3" id="jobTitle04">募集職種名</h3>
          <div class="p-requirements--table">
            <table class="table">
              <tr>
                <th>仕事内容</th>
                <td>一級建築施工管理技士資格を活かし公共事業の現場でご活躍いただきます。建築職として管理施設の新築・増改築工事における、発注者支援業務の施工計画・工程・安全・品質・予算管理の補助を行っていただきます。資格を活かした業務を担当いただきますが、詳細は各現場によって異なるため、面接にてお伺いいただければと思います。 <br>[例]高速道路建設、区画整理事業の施工など <br>工期も案件によって異なり、半年～長いと7,8年と様々です。将来的にはメインでCM業務を担当いただきます。</td>
              </tr>
              <tr>
                <th>必要な経験・能力等</th>
                <td>[必須] 一級建築施工管理技士資格<br>[歓迎] 公共工事に関わる経験がある方</td>
              </tr>
              <tr>
                <th>給与</th>
                <td>月給: 330,000～500,000</td>
              </tr>
              <tr>
                <th>対象</th>
                <td>中途、新卒</td>
              </tr>
              <tr>
                <th>雇用契約</th>
                <td>正社員<br>期間の定め: 無し</td>
              </tr>
              <tr>
                <th>賞与</th>
                <td>年2回</td>
              </tr>
              <tr>
                <th>勤務時間</th>
                <td>8:00～16:25 (所定労働時間: 7時間40分)</td>
              </tr>
              <tr>
                <th>勤務地</th>
                <td>本社: 東京メトロ丸ノ内線「荻窪」駅 徒歩3分<br>※勤務地は各現場となります。敷地内禁煙(屋内喫煙可能場所あり)<br> 転勤有</td>
              </tr>
              <tr>
                <th>休日</th>
                <td>124日 (土日祝、夏季休暇3日、年末年始6日)<br>有給休暇: 10～20日 (全労働日の8割以上出勤した場合)</td>
              </tr>
              <tr>
                <th>各種保険</th>
                <td>健康保険、厚生年金保険、雇用保険、労災保険</td>
              </tr>
              <tr>
                <th>制度・福利厚生</th>
                <td>家族手当: 10,000円～<br>職責手当: 2,000円～</td>
              </tr>
            </table>
          </div><!-- ./p-requirements--table -->    
          <div class="p-requirements--entry">
            <a href="" class="btn-view-more5">ENTRY<span>エントリー</span></a>
          </div>
        </div><!-- ./p-requirements--row -->
        <div class="p-requirements--row" id="job05">
          <h3 class="section-ttl3" id="jobTitle05">募集職種名</h3>
          <div class="p-requirements--table">
            <table class="table">
              <tr>
                <th>仕事内容</th>
                <td>一級建築施工管理技士資格を活かし公共事業の現場でご活躍いただきます。建築職として管理施設の新築・増改築工事における、発注者支援業務の施工計画・工程・安全・品質・予算管理の補助を行っていただきます。資格を活かした業務を担当いただきますが、詳細は各現場によって異なるため、面接にてお伺いいただければと思います。 <br>[例]高速道路建設、区画整理事業の施工など <br>工期も案件によって異なり、半年～長いと7,8年と様々です。将来的にはメインでCM業務を担当いただきます。</td>
              </tr>
              <tr>
                <th>必要な経験・能力等</th>
                <td>[必須] 一級建築施工管理技士資格<br>[歓迎] 公共工事に関わる経験がある方</td>
              </tr>
              <tr>
                <th>給与</th>
                <td>月給: 330,000～500,000</td>
              </tr>
              <tr>
                <th>対象</th>
                <td>中途、新卒</td>
              </tr>
              <tr>
                <th>雇用契約</th>
                <td>正社員<br>期間の定め: 無し</td>
              </tr>
              <tr>
                <th>賞与</th>
                <td>年2回</td>
              </tr>
              <tr>
                <th>勤務時間</th>
                <td>8:00～16:25 (所定労働時間: 7時間40分)</td>
              </tr>
              <tr>
                <th>勤務地</th>
                <td>本社: 東京メトロ丸ノ内線「荻窪」駅 徒歩3分<br>※勤務地は各現場となります。敷地内禁煙(屋内喫煙可能場所あり)<br> 転勤有</td>
              </tr>
              <tr>
                <th>休日</th>
                <td>124日 (土日祝、夏季休暇3日、年末年始6日)<br>有給休暇: 10～20日 (全労働日の8割以上出勤した場合)</td>
              </tr>
              <tr>
                <th>各種保険</th>
                <td>健康保険、厚生年金保険、雇用保険、労災保険</td>
              </tr>
              <tr>
                <th>制度・福利厚生</th>
                <td>家族手当: 10,000円～<br>職責手当: 2,000円～</td>
              </tr>
            </table>
          </div><!-- ./p-requirements--table -->    
          <div class="p-requirements--entry">
            <a href="" class="btn-view-more5">ENTRY<span>エントリー</span></a>
          </div>
        </div><!-- ./p-requirements--row --> 
      </div>
    </div>
  </div>
</main><!-- ./main -->

<div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li><a href="/recruit/">採用情報</a></li>
      <li>募集要項</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>