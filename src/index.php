<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-top.php'; ?>
<main class="main top js-main">
  <div class="mv-overlayWrap">
      <div class="mv-overlay js-mvOverlay"></div>
  </div>
  <div class="mv-matrixWrap">
    <div class="mv-matrix js-mvMatrix"></div>
  </div>
  <div class="mv-logo js-logo">
    <img src="<?php echo $PATH;?>/assets/images/common/logo-loading.svg" alt="">
  </div>
  <!-- <div class="mv-img js-mvImg"></div> -->

  <section class="section-about">
    <div class="section-about--intro-wrap">
      <div class="scroll js-scroll">
        <a href="javascript:void(0)">Scroll</a>
      </div>
      <div class="section-about--intro-video">
        <div class="video-container js-mvImg">
          <div id="ytplayer"></div>
        </div>
      </div>
      <div class="section-about--intro js-aboutIntro">
        <p class="section-about--intro-ttl">測量・調査からコンサルタントまでまちづくりをトータルサポート</p>
        <p class="section-about--intro-cnt">
          未来を見据えた、豊かなまちづくりには、高度な測量技術による地域の特長と課題を把握することが重要です。 
          <br><br>エープランニングは、お客様のパートナーシップカンパニーとして、高速道路の建設や区画整理など、公共事業に伴う測量・調査から補償・建設のコンサルタント業務、技術者の人材派遣まで、トータルサポートするサービスをご提供します。こちらは仮テキストです。
        </p>
      </div>
    </div>
    <div class="section-about--reason">
      <div class="section-about--reason-inner">
        <p class="section-about--reason-heading fadeup">エープラニングは、お客様を総合力でサポートします</p>
        <ul class="section-about--reason-list">
          <li class="fadeup">
            <div class="section-about--reason-link">
              <p class="section-about--reason-label">REASON 01</p>
              <div class="section-about--reason-ttlWrap">
                <div class="section-about--reason-icon">
                  <img class="pc-only" src="<?php echo $PATH;?>/assets/images/top/reason01.svg" alt="">
                  <img class="sp-only" src="<?php echo $PATH;?>/assets/images/top/reason01-sp.svg" alt="">
                </div>
                <h3 class="section-about--reason-ttl">測量・調査から施工までトータルサポート</h3>
              </div>
              <p class="section-about--reason-des">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
            </div>
          </li>
          <li class="fadeup delay-2">
            <div class="section-about--reason-link">
              <p class="section-about--reason-label">REASON 02</p>
              <div class="section-about--reason-ttlWrap">
                <div class="section-about--reason-icon">
                  <img class="pc-only" src="<?php echo $PATH;?>/assets/images/top/reason02.svg" alt="">
                  <img class="sp-only" src="<?php echo $PATH;?>/assets/images/top/reason02-sp.svg" alt="">
                </div>
                <h3 class="section-about--reason-ttl">測量・調査から施工までトータルサポート</h3>
              </div>
              <p class="section-about--reason-des">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
            </div>
          </li>
          <li class="fadeup delay-4">
            <div class="section-about--reason-link">
              <p class="section-about--reason-label">REASON 03</p>
              <div class="section-about--reason-ttlWrap">
                <div class="section-about--reason-icon">
                  <img class="pc-only" src="<?php echo $PATH;?>/assets/images/top/reason03.svg" alt="">
                  <img class="sp-only" src="<?php echo $PATH;?>/assets/images/top/reason03-sp.svg" alt="">
                </div>
                <h3 class="section-about--reason-ttl">測量・調査から施工までトータルサポート</h3>
              </div>
              <p class="section-about--reason-des">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
            </div>
          </li>
        </ul><!-- ./section-about--reason-list -->
        <div class="align-center fadeup"><a href="/" class="btn-view-more">当社の強み</a></div>
      </div><!-- ./section-about--reason-inner -->
    </div>
  </section><!-- ./section-about -->

  <section class="section-service">
    <div class="section-service--bg">
      <div class="section-service--bg-img fadein delay-8">
        <img src="<?php echo $PATH;?>/assets/images/top/bg-service01.png" alt="">
      </div>
      <div class="section-service--bg-img fadein delay-12">
        <img src="<?php echo $PATH;?>/assets/images/top/bg-service02.png" alt="">
      </div>
      <div class="section-service--bg-img fadein delay-16">
        <img src="<?php echo $PATH;?>/assets/images/top/bg-service03.png" alt="">
      </div>
    </div>
    <div class="section-service--inner fadeup delay-18">
      <h2 class="section-service--ttl">SERVICE<span>事業内容</span></h2>
      <div class="section-service--intro">
        <p class="section-service--intro-cnt">エープランニングは、お客様のパートナーシップカンパニーとして、高速道路の建設や区画整理など、公共事業に伴う測量・調査から補償・建設のコンサルタント業務、技術者の人材派遣まで、トータルサポートするサービスをご提供します。</p>
        <a href="/" class="btn-view-more">事業内容</a>
      </div><!-- ./section-service--intro -->
    </div>
  </section><!-- ./section-service -->

  <section class="section-news fadeup">
    <h2 class="section-news--ttl">NEWS</h2>
    <div class="section-news--inner">
      <div class="section-news--cnt">
        <ul class="section-news--list">
          <li class="section-news--item">
            <a class="link" href="" target="_blank">
              <div class="section-news--item-infor">
                <span class="date">2019.00.00</span>
                <p class="desc"><span class="link-pdf">中途採用募集（土地家屋調査士）のお知らせ</span></p>
              </div>
              <!-- <div class="tagWrap">
                <span class="tag">RECRUIT</span>
              </div> -->
            </a>
          </li>
          <li class="section-news--item">
            <a class="link" href="" target="_blank">
              <div class="section-news--item-infor">
                <span class="date">2019.00.00</span>
                <p class="desc"><span class="link-blank">中途採用募集(土地家屋調査士)のお知らせ</span></p>
              </div>
              <!-- <div class="tagWrap">
                <span class="tag">RECRUIT</span>
              </div> -->
            </a>
          </li>
          <li class="section-news--item">
            <a class="link" href="">
              <div class="section-news--item-infor">
                <span class="date">2019.00.00</span>
                <p class="desc">中途採用募集（土地家屋調査士）のお知らせ</p>
              </div>
              <div class="tagWrap">
                <span class="tag">RECRUIT</span>
              </div>
            </a>
          </li>
          <li class="section-news--item">
            <a class="link" href="">
              <div class="section-news--item-infor">
                <span class="date">2019.00.00</span>
                <p class="desc">新型コロナウイルス感染症の拡大防止への対応状況について</p>
              </div>
              <div class="tagWrap">
                <span class="tag">NEWS</span>
              </div>
            </a>
          </li>
          <li class="section-news--item">
            <a class="link" href="">
              <div class="section-news--item-infor">
                <span class="date">2019.00.00</span>
                <p class="desc">「測量・調査」の実績を更新しました。</p>
              </div>
              <div class="tagWrap">
                <span class="tag">WORKS</span>
              </div>
            </a>
          </li>
        </ul>
        <div class="view-moreWrap"><a href="/" class="view-more">一覧を見る</a></div>
      </div><!-- ./section-news -->
    </div>
  </section><!-- ./section-news -->

  <section class="section-direct fadeup">
    <div class="section-direct--list">
      <div class="section-direct--item">
        <a class="link" href="/recruit">
          <div class="section-direct--label">
            <div class="section-direct--label-inner">
              <span>RECRUIT</span>
            </div>
          </div>
          <div class="section-direct--icon"></div>
          <div class="section-direct--thumb">
            <img src="<?php echo $PATH;?>/assets/images/top/recruit.jpg" alt="">
          </div>
          <div class="section-direct--infor">
            <div class="section-direct--infor-inner">
              <p class="section-direct--infor-ttl">採用情報</p>
              <p class="section-direct--infor-des pc-only">&nbsp;</p>
            </div>
          </div>
        </a>
      </div>
      <div class="section-direct--item">
        <a class="link" href="/philosophy">
          <div class="section-direct--label">
            <div class="section-direct--label-inner">
              <span>COMPANY</span>
            </div>
          </div>
          <div class="section-direct--iconWrap">
            <div class="section-direct--icon"></div>
          </div>
          <div class="section-direct--thumb">
            <img src="<?php echo $PATH;?>/assets/images/top/company.jpg" alt="">
          </div>
          <div class="section-direct--infor">
            <div class="section-direct--infor-inner">
              <p class="section-direct--infor-ttl">会社情報</p>
              <p class="section-direct--infor-des">本社アクセス情報はこちら</p>
            </div>
          </div>
        </a>
      </div>
    </div><!-- ./section-direct--list -->
  </section><!-- ./section-direct -->

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-top.php'; ?>