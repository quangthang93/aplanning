<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="p-end p-recruit">
    <div class="p-end--banner">
      <div class="container">
        <h1 class="main-ttl">SPECIAL<span>数字で見るエープランニング</span></h1>
      </div>
    </div><!-- ./p-recruit--banner -->
    <section class="data data__box-top">
      <h3>社員の声を聞いてみよう！</h3>
      <img class="pc-m-only" src="<?php echo $PATH;?>/assets/images/data/top01.svg" alt="社員の声を聞いてみよう！">
      <img class="sp-m-only" src="<?php echo $PATH;?>/assets/images/data/top-sp.svg" alt="社員の声を聞いてみよう！">
      
    </section>
    <section class="data data__col">
      <h2>どんな会社？</h2>
      <div class="list__row fadeup ">
        <div class="row">
          <div class="col-3">
            <div class="data__col--box">
              <h3>売上成長率</h3>
              <h4>Sales growth rate</h4>
              <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/col01.svg" alt="売上成長率"></div>
              <p>売上高は設立より、毎年増加し続けています。</p>
            </div>
          </div>
          <div class="col-9">
            <div class="data__col--box">
              <ul>
                <li>
                  <h3>従業員数</h3>
                  <h4>number of employees</h4>
                  <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/col02.svg" alt="従業員数"></div>
                </li>
                <li>
                  <h3>従業員増加率</h3>
                  <h4>Employee growth rate</h4>
                  <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/col03.svg" alt="従業員増加率"></div>
                </li>
              </ul>
              <p>売上成長に比例して、従業員数も増加中。採用も積極的に行なっています。</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-3">
            <div class="data__col--box">
              <h3>資格保有者率</h3>
              <h4>Qualification holder rate</h4>
              <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/occ03.svg" alt="資格保有者率"></div>
              <div class="list__check">
                <h5>（一部抜粋）</h5>
                <div class="list__check--item">
                  <ul>
                    <li><span><img src="<?php echo $PATH;?>/assets/images/data/check.svg"></span>測量士・測量士補</li>
                    <li><span><img src="<?php echo $PATH;?>/assets/images/data/check.svg"></span>土地家屋調査士</li>
                    <li><span><img src="<?php echo $PATH;?>/assets/images/data/check.svg"></span>補償業務管理士</li>
                  </ul>
                  <ul>
                    <li><span><img src="<?php echo $PATH;?>/assets/images/data/check.svg"></span>技術士・技術士補</li>
                    <li><span><img src="<?php echo $PATH;?>/assets/images/data/check.svg"></span>一級・二級建築士</li>
                    <li><span><img src="<?php echo $PATH;?>/assets/images/data/check.svg"></span>施工管理技士</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-9">
            <div class="data__col--box">
              <ul>
                <li>
                  <h3>職種の比率</h3>
                  <h4>Occupation ratio</h4>
                  <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/occ.svg" alt="職種の比率"></div>
                </li>
                <li>
                  <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/occ02.svg" alt="職種の比率"></div>
                </li>
              </ul>
              <p>ダミーテキストダミーテキストダミーテキストダミーテキスト。</p>
            </div>
          </div>
        </div>
      </div>
      <h2>どんな人が働いている？</h2>
      <div class="list__row type02 fadeup">
        <div class="row">
          <div class="col-3">
            <div class="data__col--box">
              <h3>文理の比率</h3>
              <h4>Literary ratio</h4>
              <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/lite.svg" alt="文理の比率"></div>
              <p>売上高は設立より、毎年増加し続けています。</p>
            </div>
          </div>
          <div class="col-9">
            <div class="data__col--box">
              <ul>
                <li>
                  <h3>平均年齢</h3>
                  <h4>Average age</h4>
                  <div class="img pc-m-only"><img src="<?php echo $PATH;?>/assets/images/data/year01.svg" alt="平均年齢"></div>
                  <div class="img sp-m-only"><img src="<?php echo $PATH;?>/assets/images/data/year01-sp.png" alt="平均年齢"></div>
                </li>
                <li>
                  <h3>年齢分布</h3>
                  <h4>Age distribution</h4>
                  <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/char.svg" alt="平均年齢"></div>
                </li>
              </ul>
              <p>売上成長に比例して、従業員数も増加中。採用も積極的に行なっています。</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-3">
            <div class="data__col--box">
              <h3>血液型</h3>
              <h4>blood type</h4>
              <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/qua.svg" alt="資格保有者率"></div>
              <p>ダミーテキストダミーテキストダミー</p>
            </div>
          </div>
          <div class="col-9">
            <div class="data__col--box">
              <h3>出身地</h3>
              <h4>Birthplace</h4>
              <ul>
                <li>
                  
                  <p>ダミーテキストダミーテキストダミーテキストダミーテキスト。ダミーテキストダミーテキストダミー</p>
                </li>
                <li>
                  <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/bir02.svg" alt="出身地"></div>
                </li>
              </ul>
              
            </div>
          </div>
        </div>
      </div>
      <h2>働きやすい制度や社風</h2>
      <div class="data__style fadeup">
        <div class="row">
          <div class="col-5">
            <div class="data__style--info">
              <h3>定時退社率</h3>
              <div class="list__block">
                <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/tokei.svg"></div>
                <div class="list--red">
                  <ul class="red--text">
                    <li>85</li>
                    <li>%</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-5">
            <div class="data__style--info">
              <h3>平均残業時間</h3>
              <div class="list__block">
                <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/cal.svg"></div>
                <div class="list--red">
                  <ul class="red--text">
                    <li>15</li>
                    <li>時間/月</li>
                  </ul>
                  <p>2020年度実績</p>
                </div>
              </div>
              
            </div>
          </div>
          <div class="col-5">
            <div class="data__style--info">
              <h3>年間休日日数</h3>
              <div class="list__block">
                <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/bea.svg"></div>
                <div class="list--red">
                  <ul class="red--text">
                    <li>125</li>
                    <li>日</li>
                  </ul>
                  <p>2020年度実績</p>
                </div>
              </div>
              
            </div>
          </div>
          <div class="col-5">
            <div class="data__style--info">
              <h3>有給取得率</h3>
              <div class="list__block">
                <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/not.svg"></div>
                <div class="list--red">
                  <ul class="red--text">
                    <li>100</li>
                    <li>％</li>
                  </ul>
                  <p>2020年度実績</p>
                </div>
              </div>
              
            </div>
          </div>
          <div class="col-5">
            <div class="data__style--info">
              <h3>離職率</h3>
              <div class="list__block">
                <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/com.svg"></div>
                <div class="list--red">
                  <ul class="red--text">
                    <li>2</li>
                    <li>％</li>
                  </ul>
                  <p>3年以内の離職率</p>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
      <div class="data__heath fadeup">
        <div class="row">
          <div class="col-4">
            <div class="data__col--box">
              <h3>平均勤続年数</h3>
              <h4>Average years of service</h4>
              <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/com02.svg"></div>
            </div>
          </div>
          <div class="col-4">
            <div class="data__col--box">
              <h3>産休育休復職率</h3>
              <h4>Maternity leavey</h4>
              <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/hea.svg"></div>
              <p>2020年度実績</p>
            </div>
          </div>
          <div class="col-4">
            <div class="data__col--box">
              <h3>健康診断受診率</h3>
              <h4>Health checkup rate</h4>
              <div class="img"><img src="<?php echo $PATH;?>/assets/images/data/hea02.svg"></div>
              
            </div>
          </div>
        </div>
      </div>
      <div class="data__document fadeup">
        <div class="data__col--box">
          <h3>年間資格取得率</h3>
          <h4>Annual qualification acquisition rate</h4>
          <ul class="list-style">
            <li>（仮）社員の資格取得を積極的に支援するため、会社が推奨する資格のうち、資格手当対象外の資格を“奨励資格”とし、報奨金や受験料補助を行っています。</li>
            <li>
              <img class="pc-m-only" src="<?php echo $PATH;?>/assets/images/data/doc.png">
              <img class="sp-m-only" src="<?php echo $PATH;?>/assets/images/data/cup-sp.png">
            </li>
            <li>
              <div class="list__check">
                <h3>取得が推奨されている資格</h3>
                <h5>（一部抜粋）</h5>
                <div class="list__check--item">
                  <ul>
                    <li><span><img src="<?php echo $PATH;?>/assets/images/data/check.svg"></span>測量士・測量士補</li>
                    <li><span><img src="<?php echo $PATH;?>/assets/images/data/check.svg"></span>土地家屋調査士</li>
                    <li><span><img src="<?php echo $PATH;?>/assets/images/data/check.svg"></span>補償業務管理士</li>
                    <li><span><img src="<?php echo $PATH;?>/assets/images/data/check.svg"></span>技術士・技術士補</li>
                    <li><span><img src="<?php echo $PATH;?>/assets/images/data/check.svg"></span>一・二級建築士</li>
                    <li><span><img src="<?php echo $PATH;?>/assets/images/data/check.svg"></span>施工管理技士</li>
                  </ul>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <div class="list__tag fadeup">
        <ul>
          <li><a href="/recruit/#ourpeople">社員インタビュー</a></li>
          <li><a href="/recruit/#joblist">募集要項</a></li>
        </ul>
      </div>
    </section>
    <section class="p-recruit--entry fadeup">
      <div class="container">
        <h2 class="section-ttl">ENTRY<span>応募方法</span></h2>
        <p class="p-recruit--entry-summary">当社に興味をお持ちの方は、下記の連絡先よりお問い合わせください。折り返し、採用担当者からご連絡します。</p>
        <div class="p-recruit--entry-inforWrap">
          <div class="p-recruit--entry-infor">
            <p class="p-recruit--entry-infor-ttl">採用に関するお問い合わせ先</p>
            <p class="p-recruit--entry-infor-des">株式会社エープランニング　〇〇部 採用担当(担当:大熊)<br>〒 167-0043　東京都杉並区上荻一丁目15番1号<br>TEL：03-5335-5828<br>E-Mail：info@a-planning-inc.co.jp</p>
          </div>
          <div class="p-recruit--entry-direct">
            <a href="" class="btn-view-more5">ENTRY<span>エントリー</span></a>
          </div>
        </div>
      </div>
    </section>
  </div>
</main><!-- ./main -->

<div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>数字で見るエープランニング</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>