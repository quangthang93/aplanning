<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="p-end p-businessDetail">
    <section class="p-end--banner">
      <div class="container">
        <h1 class="main-ttl">SERVICE<span>事業内容</span></h1>
      </div>
    </section><!-- ./p-recruit--banner -->
    <section class="p-businessDetail--cnt">
      <div class="p-businessDetail--ttlWrap">
        <div class="container">
          <h2 class="section-ttl2">各種一般測量</h2>
        </div>
      </div>
      <div class="p-businessDetail--intro">
        <div class="container">
          <div class="p-businessDetail--intro-ttlWrap">
            <h3 class="p-businessDetail--intro-ttl">こちらに事業に関するボディーコピーが入ります</h3>
          </div>
          <p class="p-businessDetail--intro-des">これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。</p>
        </div>
      </div>
      <div class="p-businessDetail--points">
        <div class="container">
          <ul class="section-about--reason-list">
            <li>
              <div class="section-about--reason-link">
                <p class="section-about--reason-label">POINT 01</p>
                <div class="section-about--reason-ttlWrap">
                  <h3 class="section-about--reason-ttl">この事業に関する選ばれる理由・強みのポイント</h3>
                </div>
                <p class="section-about--reason-des">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
              </div>
            </li>
            <li>
              <div class="section-about--reason-link">
                <p class="section-about--reason-label">POINT 02</p>
                <div class="section-about--reason-ttlWrap">
                  <h3 class="section-about--reason-ttl">この事業に関する選ばれる理由・強みのポイント</h3>
                </div>
                <p class="section-about--reason-des">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
              </div>
            </li>
            <li>
              <div class="section-about--reason-link">
                <p class="section-about--reason-label">POINT 03</p>
                <div class="section-about--reason-ttlWrap">

                  <h3 class="section-about--reason-ttl">この事業に関する選ばれる理由・強みのポイント</h3>
                </div>
                <p class="section-about--reason-des">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
              </div>
            </li>
          </ul><!-- ./section-about--reason-list -->
        </div>
      </div><!-- p-businessDetail--points -->    
      <div class="p-businessDetail--cnt-inner">
        <div class="container">
          <div class="p-businessDetail--cnt-row">
            <h2 class="section-ttl2">主な業務内容</h2>
            <ul class="p-businessDetail--list">
              <li class="p-businessDetail--item">
                <h4 class="p-businessDetail--item-ttl">基準点測量</h4>
                <div class="p-businessDetail--item-desWrap">
                  <p class="p-businessDetail--item-des">基準点とは、地球上の位置や海面からの高さが正確に測定された三角点、電子基準点等をいい、すべての測量の基礎となるもので、公共測量等に使用されます。<br>基準点測量は、既設の基準点等に基づき、新たに基準点を設置する場合にその設置位置や高さを求める作業で、用地図の作成や道路の建設、都市開発などの様々な公共事業等を行うための基礎となるものです。観測にはGNSS測量機やトータルステーション等を用いて行います。</p>
                </div>
              </li>
              <li class="p-businessDetail--item">
                <h4 class="p-businessDetail--item-ttl">水準測量</h4>
                <div class="p-businessDetail--item-desWrap">
                  <p class="p-businessDetail--item-des">国会議事堂に隣接する国会前庭に日本水準原点が設置されています。現在の標高は24.3900mとなっており、その標高をもちいて日本各地に水準点が配置されており、河川や道路、港湾、下水道、鉄道等の正確な高さの値が必要な工事で測量の基準として用いられています。観測にはレベル、標尺を用いて関係点間の高低差を観測し、新点である水準点の標高を求めます。2地点間の高低差を直接観測する「直接水準測量」、トータルステーションを用いて2地点間の高低差を角度と距離を観測して間接的に求める「間接水準測量」、GNSS測量機を用いるスタティック法で高精度に観測が行える「GNSS水準測量」があります。</p>
                </div>
              </li>
              <li class="p-businessDetail--item">
                <h4 class="p-businessDetail--item-ttl">地形測量</h4>
                <div class="p-businessDetail--item-desWrap">
                  <p class="p-businessDetail--item-des">現況地形のデータを取得するために、基準点にトータルステーションなどを設置して地形、構造物などの位置及び形状を測定し、数値地形図データを作成します。技術の進歩により、現在はUAVレーザ測量システムやMMS(車載写真レーザ測量システム)、地上型レーザスキャナなどの最新機器を活用することで、計測精度の確保や向上、ならびに作業の迅速化が図られています。このように、二次元あるいは三次元計測により作成した詳細な地形図(平面図)のデータは、公共土木施設等事業の計画、設計、工事、防災及び環境など様々な用途に利用されます。</p>
                </div>
              </li>
              <li class="p-businessDetail--item">
                <h4 class="p-businessDetail--item-ttl">路線測量</h4>
                <div class="p-businessDetail--item-desWrap">
                  <p class="p-businessDetail--item-des">路線測量は築造物(道路・水路等)延長の長い構造物を新築・改築する際の、設計業務に必要となる横断面図や縦断面図などを作成する測量です。中心線の設置、地形変化点の観測、縦横断図の作成、用地幅杭設置測量等を行います。</p>
                </div>
              </li>
              <li class="p-businessDetail--item">
                <h4 class="p-businessDetail--item-ttl">河川測量</h4>
                <div class="p-businessDetail--item-desWrap">
                  <p class="p-businessDetail--item-des">河川、ダム、貯水池、港湾の形状、水位、深浅、断面、勾配等を測定し、平面図、縦断面図、横断面図などの作成や、流速、流量などを調査します。調査の性質から定期的に継続して行うもので、その成果は治水、利水の総合計画の資料として用いられるほか、湖沼、海岸等の保全のための測量です。</p>
                </div>
              </li>
            </ul>
          </div><!-- p-businessDetail--cnt-row -->  
          <div class="p-businessDetail--cnt-row mgt-90">
            <h2 class="section-ttl2 mgb-50">主な実績</h2>
            <p class="p-businessDetail--item-des mgb-50">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字</p>
          </div><!-- p-businessDetail--cnt-row -->  
        </div>
      </div>  
      <div class="p-businessDetail--direct">
        <div class="container align-center">
          <a href="" class="btn-direct"><span>国家資格有資格者一覧</span></a>
          <!-- <div class="p-businessDetail--direct-list">
            <div class="p-businessDetail--direct-item">
              <a class="link" href="">
                <div class="p-businessDetail--direct-infor">
                  <div class="p-businessDetail--direct-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/common/icon-book.svg" alt="">
                  </div>
                  <p class="p-businessDetail--direct-ttl">主な実績</p>
                </div>
              </a>
            </div>
            <div class="p-businessDetail--direct-item">
              <a class="link" href="">
                <div class="p-businessDetail--direct-infor">
                  <div class="p-businessDetail--direct-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/common/icon-certi.svg" alt="">
                  </div>
                  <p class="p-businessDetail--direct-ttl">国家資格有資格者一覧</p>
                </div>
              </a>
            </div>
          </div> -->
        </div>
      </div>
      <!-- ./p-businessDetail--direct -->
      <div class="p-businessDetail--contact">
        <div class="container">
          <div class="p-businessDetail--contact-inner">
            <p class="p-businessDetail--contact-label">お問い合わせ先</p>
            <div class="p-businessDetail--contact-box">
              <p class="p-businessDetail--contact-ttl">当社の事業・サービスに関するお問い合わせ、ご相談は下記の連絡先までお願いいたします。</p>
              <div class="p-businessDetail--contact-cnt">
                <div class="p-businessDetail--contact-infor">
                  <p class="p-businessDetail--contact-infor-name">株式会社エープランニング〇〇部</p>
                  <p class="p-businessDetail--contact-infor-phone"><a href="TEL:03-5335-5828">TEL:03-5335-5828</a></p>
                  <p class="p-businessDetail--contact-infor-time">[受付時間]平日9:00～18:00</p>
                </div>
                <div class="p-businessDetail--contact-link">
                  <a class="btn-contact" href="/contact">お問い合わせ</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!-- ./p-businessDetail--contact -->
      <div class="view-moreWrap"><a href="/business" class="view-more3">事業内容一覧を見る</a></div>
    </section>
  </div>
</main><!-- ./main -->

<div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li><a href="/news">事業内容</a></li>
      <li>一般労働者派遣事業</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>