<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="p-end p-business">
    <div class="p-end--banner">
      <div class="container">
        <h1 class="main-ttl">SERVICE<span>事業内容</span></h1>
      </div>
    </div><!-- ./p-recruit--banner -->
    <div class="p-business--cnt">
      <div class="container">
        <p class="p-business--intro">エープランニングは、お客様のパートナーシップカンパニーとして、高速道路の建設や区画整理など、公共事業に伴う測量・調査から補償・建設のコンサルタント業務、技術者の人材派遣まで、トータルサポートするサービスをご提供します。</p>
      </div>

      <div class="p-businessDetail--points">
        <div class="container">
          <ul class="section-about--reason-list">
            <li>
              <div class="section-about--reason-link">
                <p class="section-about--reason-label">POINT 01</p>
                <div class="section-about--reason-ttlWrap">
                  <h3 class="section-about--reason-ttl">選ばれる理由・強みのポイント</h3>
                </div>
                <p class="section-about--reason-des">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
              </div>
            </li>
            <li>
              <div class="section-about--reason-link">
                <p class="section-about--reason-label">POINT 02</p>
                <div class="section-about--reason-ttlWrap">
                  <h3 class="section-about--reason-ttl">選ばれる理由・強みのポイント</h3>
                </div>
                <p class="section-about--reason-des">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
              </div>
            </li>
            <li>
              <div class="section-about--reason-link">
                <p class="section-about--reason-label">POINT 03</p>
                <div class="section-about--reason-ttlWrap">

                  <h3 class="section-about--reason-ttl">選ばれる理由・強みのポイント</h3>
                </div>
                <p class="section-about--reason-des">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
              </div>
            </li>
          </ul><!-- ./section-about--reason-list -->
        </div>
      </div><!-- p-businessDetail--points -->    
      
      <div class="container">
        <div class="p-business--list">
          <div class="p-business--item">
            <a href="/business/detail">
              <div class="p-business--item-infor">
                <h2 class="p-business--item-ttl">各種一般測量</h2>
                <p class="p-business--item-cnt">これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。これは正式な文章の代わりに入れて使うダミーテキストです。</p>
              </div>
              <div class="p-business--item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/business/business01.png" alt="">
              </div>
            </a>
          </div>
          <div class="p-business--item">
            <a href="/business/detail">
              <div class="p-business--item-infor">
                <h2 class="p-business--item-ttl">補償コンサルタント(全部門)業務</h2>
                <p class="p-business--item-cnt">これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。これは正式な文章の代わりに入れて使うダミーテキストです。</p>
              </div>
              <div class="p-business--item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/business/business02.png" alt="">
              </div>
            </a>
          </div>
          <div class="p-business--item">
            <a href="/business/detail">
              <div class="p-business--item-infor">
                <h2 class="p-business--item-ttl">土地家屋調査測量業務</h2>
                <p class="p-business--item-cnt">これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。これは正式な文章の代わりに入れて使うダミーテキストです。</p>
              </div>
              <div class="p-business--item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/business/business03.png" alt="">
              </div>
            </a>
          </div>
          <div class="p-business--item">
            <a href="/business/detail">
              <div class="p-business--item-infor">
                <h2 class="p-business--item-ttl">建設コンサルタント</h2>
                <p class="p-business--item-cnt">これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。これは正式な文章の代わりに入れて使うダミーテキストです。</p>
              </div>
              <div class="p-business--item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/business/business04.png" alt="">
              </div>
            </a>
          </div>
          <div class="p-business--item">
            <a href="/business/detail">
              <div class="p-business--item-infor">
                <h2 class="p-business--item-ttl">環境調査</h2>
                <p class="p-business--item-cnt">これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。これは正式な文章の代わりに入れて使うダミーテキストです。</p>
              </div>
              <div class="p-business--item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/business/business05.png" alt="">
              </div>
            </a>
          </div>
          <div class="p-business--item">
            <a href="/business/detail">
              <div class="p-business--item-infor">
                <h2 class="p-business--item-ttl">一般労働者派遣事業</h2>
                <p class="p-business--item-cnt">これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。これは正式な文章の代わりに入れて使うダミーテキストです。</p>
              </div>
              <div class="p-business--item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/business/business06.png" alt="">
              </div>
            </a>
          </div>
          <div class="p-business--item">
            <a href="/business/detail">
              <div class="p-business--item-infor">
                <h2 class="p-business--item-ttl">有料職業紹介事業</h2>
                <p class="p-business--item-cnt">これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。これは正式な文章の代わりに入れて使うダミーテキストです。</p>
              </div>
              <div class="p-business--item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/business/business07.png" alt="">
              </div>
            </a>
          </div>
        </div><!-- ./p-business--list -->
      </div>
    </div>
  </div>
</main><!-- ./main -->
<div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>事業内容</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>