<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="p-end p-businessDetail">
    <section class="p-end--banner">
      <div class="container">
        <h1 class="main-ttl">SERVICE<span>事業内容</span></h1>
      </div>
    </section><!-- ./p-recruit--banner -->
    <section class="p-businessDetail--cnt">
      <div class="p-businessDetail--ttlWrap type2">
        <div class="container">
          <h2 class="section-ttl2">一般労働者派遣事業</h2>
        </div>
      </div>
      <div class="p-businessDetail--intro">
        <div class="container">
          <div class="p-businessDetail--intro-ttlWrap">
            <h3 class="p-businessDetail--intro-ttl">お客様のニーズに応じた技術者派遣で業務をサポートいたします。</h3>
          </div>
          <p class="p-businessDetail--intro-des">調査・測量・補償コンサルタント・建設コンサルタントの4つの業務領域全てに関するフルラインナップサービスを行っているエープランニングでは、その確かな技術を基に公共事業主等へ技術支援を行っております。特に道路の建設から維持・保全事業をお手伝いさせて頂いております。</p>
        </div>
      </div>
      <div class="p-businessDetail--points">
        <div class="container">
          <ul class="section-about--reason-list">
            <li>
              <div class="section-about--reason-link">
                <p class="section-about--reason-label">POINT 01</p>
                <div class="section-about--reason-ttlWrap">
                  <h3 class="section-about--reason-ttl">この事業に関する選ばれる理由・強みのポイント</h3>
                </div>
                <p class="section-about--reason-des">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
              </div>
            </li>
            <li>
              <div class="section-about--reason-link">
                <p class="section-about--reason-label">POINT 02</p>
                <div class="section-about--reason-ttlWrap">
                  <h3 class="section-about--reason-ttl">この事業に関する選ばれる理由・強みのポイント</h3>
                </div>
                <p class="section-about--reason-des">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
              </div>
            </li>
            <li>
              <div class="section-about--reason-link">
                <p class="section-about--reason-label">POINT 03</p>
                <div class="section-about--reason-ttlWrap">

                  <h3 class="section-about--reason-ttl">この事業に関する選ばれる理由・強みのポイント</h3>
                </div>
                <p class="section-about--reason-des">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
              </div>
            </li>
          </ul><!-- ./section-about--reason-list -->
        </div>
      </div><!-- p-businessDetail--points -->    
      <div class="p-businessDetail--cnt-inner">
        <div class="container">
          <h2 class="section-ttl2">対応可能な業種・業務内容</h2>
          <ul class="p-businessDetail--list">
            <li class="p-businessDetail--item">
              <h4 class="p-businessDetail--item-ttl">測量</h4>
              <div class="p-businessDetail--item-desWrap">
                <p class="p-businessDetail--item-des">これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。</p>
                <div class="table">
                  <table>
                    <tbody>
                      <tr>
                        <th>業務内容</th>
                        <td>
                          <ul>
                            <li>高速道路建設に伴う事業認定業務</li>
                            <li>高速道路用地買収に伴う測量図面等の審査、用地交渉補助</li>
                          </ul>
                        </td>
                      </tr>
                      <tr>
                        <th>派遣可能な技術者</th>
                        <td>
                          <ul>
                            <li>測量士</li>
                            <li>補償業務管理士</li>
                            <li>その他</li>
                          </ul>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </li>
            <li class="p-businessDetail--item">
              <h4 class="p-businessDetail--item-ttl">補償</h4>
              <div class="p-businessDetail--item-desWrap">
                <p class="p-businessDetail--item-des">これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。</p>
                <div class="table">
                  <table>
                    <tbody>
                      <tr>
                        <th>業務内容</th>
                        <td>
                          <ul>
                            <li>高速道路建設に伴う事業損失、家屋移転等の補償業務補助</li>
                            <li>区画整理に伴う事業損失、家屋移転等の補償業務補助</li>
                          </ul>
                        </td>
                      </tr>
                      <tr>
                        <th>派遣可能な技術者</th>
                        <td>
                          <ul>
                            <li>測量士</li>
                            <li>補償業務管理士</li>
                            <li>その他</li>
                          </ul>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </li>
            <li class="p-businessDetail--item">
              <h4 class="p-businessDetail--item-ttl">建築</h4>
              <div class="p-businessDetail--item-desWrap">
                <p class="p-businessDetail--item-des">これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。</p>
                <div class="table">
                  <table>
                    <tbody>
                      <tr>
                        <th>業務内容</th>
                        <td>
                          <ul>
                            <li>高速道路内建物建築に伴う施工管理業務</li>
                            <li>高速道路内建物の保全パトロール</li>
                          </ul>
                        </td>
                      </tr>
                      <tr>
                        <th>派遣可能な技術者</th>
                        <td>
                          <ul>
                            <li>測量士</li>
                            <li>補償業務管理士</li>
                            <li>その他</li>
                          </ul>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>  
      <div class="p-businessDetail--contact">
        <div class="container">
          <div class="p-businessDetail--contact-inner">
            <p class="p-businessDetail--contact-label">お問い合わせ先</p>
            <div class="p-businessDetail--contact-box">
              <p class="p-businessDetail--contact-ttl">当社の事業・サービスに関するお問い合わせ、ご相談は下記の連絡先までお願いいたします。</p>
              <div class="p-businessDetail--contact-cnt">
                <div class="p-businessDetail--contact-infor">
                  <p class="p-businessDetail--contact-infor-name">株式会社エープランニング〇〇部</p>
                  <p class="p-businessDetail--contact-infor-phone"><a href="TEL:03-5335-5828">TEL:03-5335-5828</a></p>
                  <p class="p-businessDetail--contact-infor-time">[受付時間]平日9:00～18:00</p>
                </div>
                <div class="p-businessDetail--contact-link">
                  <a class="btn-contact" href="/contact">お問い合わせ</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!-- ./p-businessDetail--contact -->
      <div class="view-moreWrap"><a href="/business" class="view-more3">事業内容一覧を見る</a></div>
    </section>
  </div>
</main><!-- ./main -->

<div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li><a href="/news">事業内容</a></li>
      <li>一般労働者派遣事業</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>